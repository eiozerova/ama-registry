﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ama_win
{
    public partial class TestCreditor : Form
    {
        public TestCreditor()
        {
            InitializeComponent();
        }

        const string file_path_content = "content.txt";

        private void TestCreditor_Load(object sender, EventArgs e)
        {
            try
            {
                string content = File.ReadAllText(file_path_content);
                if (!string.IsNullOrEmpty(content))
                    ucCreditor.Content = content;
            }
            catch (Exception){}
            ucCreditor.LoadHtml();
        }

        private void bOk_Click(object sender, EventArgs e)
        {
            string content= ucCreditor.Content;
            File.WriteAllText(file_path_content, content);
            Close();
        }

        private void bCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
