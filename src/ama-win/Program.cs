﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ama_win
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            string relative_path_to_index_html = @"..\..\..\ama\creditor\index.html";
            string path_to_app_exe_folder = Path.GetDirectoryName(Application.ExecutablePath);
            string path_to_index_html = Path.Combine(path_to_app_exe_folder, relative_path_to_index_html);
            TestCreditor testCreditor= new TestCreditor();
            testCreditor.ucCreditor.PathToIndex= path_to_index_html;
            Application.Run(testCreditor);
        }
    }
}
