// node optimizers\r.js -o baseUrl=. name=optimizers/almond include=forms/fms/extension out=..\built\fms.js wrap=true
({
    baseUrl: "..",
    include: ['js/forms'],
    name: "optimizers/almond",
    out: "..\\built\\forms.js",
    wrap: true,
    map:
    {
      '*':
      {
        tpl: 'js/libs/tpl',
        txt: 'js/libs/txt'
      }
    }
})