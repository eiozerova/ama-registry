require.config
({
  enforceDefine: true,
  urlArgs: "bust" + (new Date()).getTime(),
  baseUrl: '.',
  map:
  {
    '*':
    {
      txt:  'js/libs/text'
    }
  }
});

require([
	  'contents/collector'
	, 'forms/ama/contents'
], function (collect)
{
	CPW_contents = collect(
	[
		  'ama'
	], Array.prototype.slice.call(arguments, 1));

	var content_selector = $('#cpw-content-select');
	content_selector.append($('<option>', {
		value: '',
		text: ''
	}));
	for (var content_namespace in CPW_contents)
	{
		for (var content_name in CPW_contents[content_namespace])
		{
			var id_content = content_namespace + '.' + content_name;
			content_selector.append($('<option>', {
				value: id_content,
				text: id_content
			}));
		}
	}

	if (OnLoadContents)
		OnLoadContents();

	$('#contents_loaded_span').show();
});
