require.config
({
  enforceDefine: true,
  urlArgs: "bust" + (new Date()).getTime(),
  baseUrl: '.',
  paths:
  {
    styles: 'css',
    images: 'images'
  },
  map:
  {
    '*':
    {
      tpl:   'js/libs/tpl',
      css:   'js/libs/css',
      image: 'js/libs/image',
      text:  'js/libs/text'
    }
  }
});

require([
	  'forms/collector'
	, 'forms/test/index'
	, 'forms/ama/index'
], function (collect)
{
	aCPW_forms = collect(
	[
		  'test'
		, 'ama'
	], Array.prototype.slice.call(arguments, 1));
	CPW_forms = aCPW_forms;

	var form_selector = $('#cpw-form-select');
	for (var form_namespace in CPW_forms)
	{
		for (var form_name in CPW_forms[form_namespace])
		{
			var id_form = form_namespace + '.' + form_name;
			form_selector.append($('<option>', {
				value: id_form,
				text: id_form
			}));
		}
	}

	if (OnLoadForms)
		OnLoadForms();

	$('#forms_loaded_span').show();
});
