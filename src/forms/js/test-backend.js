var registry = [
	{
		Очередь: 1
		, Номер: 1
		, Кредитор: 'Иванов И И'
		, Сумма: 10000
		, Дата_включения: '01.01.2001'
	}
	, {
		Очередь: 2
		, Номер: 2
		, Кредитор: 'Петров П П'
		, Сумма: 20000
		, Дата_включения: '02.02.2002'
	}
	, {
		Очередь: 3
		, Номер: 3
		, Кредитор: 'ООО Рога и копыта'
		, Сумма: 100000
		, Вид: 'без залога'
		, Дата_включения: '03.03.2003'
	}
	, {
		Очередь: 3
		, Номер: 4
		, Кредитор: 'ЗАО Светлый путь'
		, Сумма: 300000
		, Вид: 'с залогом'
		, Дата_включения: '04.04.2003'
	}
	, {
		Очередь: 3
		, Номер: 5
		, Кредитор: 'ОАО Физалис'
		, Сумма: 12378
		, Вид: 'пени'
		, Дата_включения: '05.05.2003'
	}
	, {
		Очередь: 3
		, Номер: 6
		, Кредитор: 'НП Карбонад'
		, Сумма: 43234
		, Вид: 'штрафы'
		, Дата_включения: '06.06.2003'
	}
	, {
		Номер: 7
		, Кредитор: 'ИП Чугунов'
		, Сумма: 345534
		, Вид: 'штрафы'
		, Дата_включения: '07.07.2003'
	}
	, {
		Номер: 99999
		, Кредитор: 'НПФ Рай'
		, Сумма: 1200
		, Вид: 'пени'
		, Дата_включения: '08.08.2003'
	}


	, {
		Очередь: 1
		, Номер: 11
		, Кредитор: 'Иванов И И'
		, Сумма: 10001
		, Дата_включения: '01.01.2001'
	}
	, {
		Очередь: 2
		, Номер: 12
		, Кредитор: 'Петров П П'
		, Сумма: 20001
		, Дата_включения: '02.02.2002'
	}
	, {
		Очередь: 3
		, Номер: 13
		, Кредитор: 'ООО Рога и копыта'
		, Сумма: 100001
		, Вид: 'без залога'
		, Дата_включения: '03.03.2003'
	}
	, {
		Очередь: 3
		, Номер: 14
		, Кредитор: 'ЗАО Светлый путь'
		, Сумма: 300001
		, Вид: 'с залогом'
		, Дата_включения: '04.04.2003'
	}
	, {
		Очередь: 3
		, Номер: 15
		, Кредитор: 'ОАО Физалис'
		, Сумма: 123781
		, Вид: 'пени'
		, Дата_включения: '05.05.2003'
	}
	, {
		Очередь: 3
		, Номер: 16
		, Кредитор: 'НП Карбонад'
		, Сумма: 432341
		, Вид: 'штрафы'
		, Дата_включения: '06.06.2003'
	}
	, {
		Номер: 17
		, Кредитор: 'ИП Чугунов'
		, Сумма: 3455341
		, Вид: 'штрафы'
		, Дата_включения: '07.07.2003'
	}
	, {
		Номер: 99991
		, Кредитор: 'НПФ Рай'
		, Сумма: 12001
		, Вид: 'пени'
		, Дата_включения: '08.08.2003'
	}
];

var changedocs = [
	{
		id: 1
		, Наименование: 'определение АС'
		, Дата: '03.06.2014'
		, ДатаРегистрации: '05.06.2014'
		, ВидыИзменения: {
			  Включения: true
			, Изменения: false
			, Исключения: false
		}
		, Сумма: 100009
		, Требований: 2
		, Кредиторов: 1
		, Очередь: 3
		, Заблокировано: '4.06.2014'
		, Пояснения: 'ПАО Быстробанк'
	}
	, {
		id: 2
		, Наименование: 'представление АУ'
		, Дата: '10.06.2014'
		, ДатаРегистрации: '10.06.2014'
		, ВидыИзменения: {
			Включения: true
			, Изменения: false
			, Исключения: false
		}
		, Сумма: 10000009
		, Требований: 1009
		, Кредиторов: 95
		, Очередь: 2
		, Заблокировано: '11.06.2014'
		, Пояснения: 'задолженность по з/п'
	}
	, {
		id: 3
		, Наименование: 'определение АС'
		, Дата: '10.07.2014'
		, ДатаРегистрации: '10.07.2014'
		, ВидыИзменения: {
			Включения: false
			, Изменения: false
			, Исключения: true
		}
		, Сумма: 1009
		, Требований: 1
		, Кредиторов: 1
		, Очередь: 2
		, Проверено: '11.07.2014'
		, Пояснения: 'Иванов И И'
	}
	, {
		id: 4
		, Наименование: 'представление АУ'
		, Дата: '10.08.2014'
		, ДатаРегистрации: '10.08.2014'
		, ВидыИзменения: {
			Включения: false
			, Изменения: true
			, Исключения: false
		}
		, Сумма: 109
		, Требований: 5
		, Кредиторов: 5
		, Очередь: 2
		, Пояснения: 'погашение'
	}
];

function GetSummary()
{
	var empty_queue = function () { return { "Кредиторов": 0, "Требований": 0, "На_сумму": 0 }; };
	var res = {
		"1-я очередь": empty_queue()
		, "2-я очередь": empty_queue()
		, "3-я очередь": {
			"без залога": empty_queue()
			, "с залогом": empty_queue()
			, "пени": empty_queue()
			, "штрафы": empty_queue()
		}
		, "за реестром": {
			"пени": empty_queue()
			, "штрафы": empty_queue()
		}
	};

	var add = function (a, i)
	{
		a.Кредиторов++;
		a.Требований++;
		a.На_сумму+= i.Сумма;
	};

	for (var i = 0; i < registry.length; i++)
	{
		var item = registry[i];
		if (!item.Очередь)
		{
			add(res['за реестром'][item.Вид], item);
		}
		else
		{
			switch (item.Очередь)
			{
				case 1: add(res['1-я очередь'], item); break;
				case 2: add(res['2-я очередь'], item); break;
				case 3: add(res['3-я очередь'][item.Вид], item); break;
			}
		}
	}

	return res;
}

var url_items_prefix_registry_items = 'ama/registry/items';
var url_items_prefix_registry_changedocs = 'ama/registry/changedocs';
var url_items_registry_changedocs_delete = 'ama/registry/changedocs/delete';
var url_items_registry_changedocs_confirm = 'ama/registry/changedocs/confirm';
var url_items_registry_changedocs_block = 'ama/registry/changedocs/block';

function FixJQGridArgs(args)
{
	args.rows = parseInt(args.rows);
	args.page = parseInt(args.page);

	if (args.filters)
	{
		var filters = args.filters;
		filters = urldecode(filters);
		filters = filters.replace("\\\"", "\"");
		filters = JSON.parse(filters);
		args.filters = filters;
	}

	if (args.sidx)
		args.sidx = urldecode(args.sidx);
}

function ParseUrlItem(url)
{
	var url_without_prefix = url.substring(url_items_prefix_registry_items.length, url.length);
	var iq = url_without_prefix.indexOf('?');
	var queue_part = url_without_prefix.substring(1, iq);
	var args_part = url_without_prefix.substring(iq+1, url_without_prefix.length);

	var queue_part_parts = queue_part.split('$');

	var args = {};
	var res =
		{
			queue_name:queue_part_parts[0]
			,kind_name: 1==queue_part_parts.length ? null : queue_part_parts[1]
			,args: args
		};
	
	var args_part_parts = args_part.split('&');
	for (var i= 0; i<args_part_parts.length; i++)
	{
		var p = args_part_parts[i].split('=');
		args[p[0]] = p[1];
	}

	FixJQGridArgs(args);

	return res;
}

function right_queue(r,parsed_url)
{
	if (parsed_url.queue_name && null != parsed_url.queue_name)
	{
		switch (parsed_url.queue_name)
		{
			case '1-я очередь': return 1 == r.Очередь;
			case '2-я очередь': return 2 == r.Очередь;
			case '3-я очередь': return 3 == r.Очередь;
			case 'за реестром': return !r.Очередь;
		}
	}
	return true;
}

function right_kind(r, parsed_url)
{
	if (parsed_url.kind_name && null != parsed_url.kind_name)
	{
		return parsed_url.kind_name == r.Вид;
	}
	return true;
}

function IsOk(r,parsed_url)
{
	if (!right_queue(r, parsed_url) || !right_kind(r, parsed_url))
	{
		return false;
	}
	else
	{
		if (parsed_url && parsed_url.args && parsed_url.args.filters && parsed_url.args.filters.rules)
		{
			var rules = parsed_url.args.filters.rules;
			for (var i= 0; i<rules.length; i++)
			{
				var rule = rules[i];
				var rule_value = rule.data;
				if (!r[rule.field])
				{
					return false;
				}
				else
				{
					var row_value = r[rule.field].toString();
					if (0 != row_value.toUpperCase().indexOf(rule_value.toUpperCase()))
						return false;
				}
			}
		}
		return true;
	}
}

function urldecode(str)
{
	return decodeURIComponent((str + '').replace(/\+/g, '%20'));
}

function JQGridSort(args)
{
	var sidx = args.sidx;
	var sord = args.sord;
	return function (a, b)
	{
		var aName = '';
		var bName = '';
		if (a[sidx])
			aName = a[sidx];
		if (b[sidx])
			bName = b[sidx];
		var res = ((aName < bName) ? -1 : ((aName > bName) ? 1 : 0));
		if ('desc' == sord)
			res = -res;
		return res;
	}
}


function GetItems(url)
{
	var parsed_url = ParseUrlItem(url);

	var args = parsed_url.args;
	var rows = args.rows;
	var page = args.page;

	var filtered_rows = [];
	for (var i = 0; i < registry.length; i++)
	{
		var r = registry[i];
		if (IsOk(r, parsed_url))
			filtered_rows.push(r);
	}

	filtered_rows.sort(JQGridSort(args));

	var page_rows = [];
	for (var i = 0; i < filtered_rows.length; i++)
	{
		var r = filtered_rows[i];
		if (Math.floor(i / rows) == (page - 1))
			page_rows.push(r);
	}

	var res =
		{
			page: page // номер текущей "страницы"
			, total: Math.floor(filtered_rows.length / rows) + 1 // количество страниц "всего"
			, records: filtered_rows.length // количество записей "всего"
			, rows: page_rows // записи текущей страницы
		};
	return res;
}

function GetChangedocs(url)
{
	/*var parsed_url = ParseUrlItem(url);

	var args = parsed_url.args;
	var rows = args.rows;
	var page = args.page;

	var filtered_rows = [];
	for (var i = 0; i < registry.length; i++)
	{
		var r = registry[i];
		if (IsOk(r, parsed_url))
			filtered_rows.push(r);
	}

	filtered_rows.sort(JQGridSort(args));
	*/

	var rows = 10;
	var page = 1;
	var filtered_rows = changedocs;

	var page_rows = [];
	for (var i = 0; i < filtered_rows.length; i++)
	{
		var r = filtered_rows[i];
		if (Math.floor(i / rows) == (page - 1))
			page_rows.push(r);
	}

	var res =
		{
			page: page // номер текущей "страницы"
			, total: Math.floor(filtered_rows.length / rows) + 1 // количество страниц "всего"
			, records: filtered_rows.length // количество записей "всего"
			, rows: page_rows // записи текущей страницы
		};

	return res;
}

function ConfirmChangeDoc(i)
{
	console.log('ConfirmChangeDoc {');
	var changedoc = changedocs[i];
	console.log('ConfirmChangeDoc 1');
	if (!changedoc.Проверено)
		changedoc.Проверено = new Date().toString();
	console.log('ConfirmChangeDoc }');
}

function BlockChangeDoc(i)
{
	ConfirmChangeDoc(i);
	var changedoc = changedocs[i];
	if (!changedoc.Заблокировано)
		changedoc.Заблокировано = new Date().toString();
}

function DeleteChangeDoc(i)
{
	console.log('DeleteChangeDoc {');
	changedocs.splice(i, 1);
	console.log('DeleteChangeDoc }');
}

function DoChangeDocId(id,dofunc)
{
	console.log('DoChangeDocId {');
	for (var i = 0; i < changedocs.length; i++)
	{
		var changedoc = changedocs[i];
		if (id == changedoc.id)
		{
			dofunc(i);
			return;
		}
	}
	console.log('DoChangeDocId }');
}

function ChangedocsGroupDo(data, dofunc)
{
	console.log('ChangedocsGroupDo {');
	var data_parts = data.split('&');
	for (var i = 0; i < data_parts.length; i++)
	{
		var data_part = data_parts[i];
		var data_part_parts = data_part.split('=');
		var id = data_part_parts[1];
		DoChangeDocId(id, dofunc);
	}
	console.log('ChangedocsGroupDo }');
}

var ajax_function= function(options)
{
	console.log('ajax_function');
	console.log(options);
	if ('ama/registry/summary' == options.url)
	{
		return GetSummary();
	}
	else if (options.url == url_items_registry_changedocs_block)
	{
		return ChangedocsGroupDo(options.data, BlockChangeDoc);
	}
	else if (options.url == url_items_registry_changedocs_confirm)
	{
		return ChangedocsGroupDo(options.data, ConfirmChangeDoc);
	}
	else if (options.url== url_items_registry_changedocs_delete)
	{
		return ChangedocsGroupDo(options.data, DeleteChangeDoc);
	}
	else if (0 == options.url.indexOf(url_items_prefix_registry_changedocs))
	{
		return GetChangedocs(options.data);
	}
	else if (0 == options.url.indexOf(url_items_prefix_registry_items))
	{
		return GetItems(options.url);
	}
	return null;
}

$(function ()
{
	$.ajaxTransport
	(
		'+*',
		function (options, originalOptions, jqXHR)
		{
			var external_ajax_transport =
			{
				send: function (headers, completeCallback)
				{
					var res = ajax_function(options);
					completeCallback(200, 'success', { text: JSON.stringify(res) });
				}
				, abort: function ()
				{
				}
			}
			return external_ajax_transport;
		}
	);
});