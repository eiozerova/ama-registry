path = require 'path'
module.exports = (grunt) ->
  require('matchdep').filterDev('grunt-*').forEach grunt.loadNpmTasks
  cfg =
    requirejs:
      baseUrl: 'js'
      paths:
        styles:       '../css'
        template:     '../templates'
        images:       '../images'
      map:
        '*':
          tpl:   'js/libs/tpl'
          css:   'libs/css'
          image: 'libs/image'
          txt:   'js/libs/text'
      shim:
        'libs/css': ['build-config']

  grunt.initConfig
    cfg: cfg

    # compress files with requireJS
    requirejs:
      forms:
        options:
          baseUrl: '.'
          paths:   cfg.requirejs.paths
          map:     cfg.requirejs.map
          shim:    cfg.requirejs.shim
          name:    'js/forms',
          out:     'built/forms.js'
      contents:
        options:
          baseUrl: '.'
          paths:   cfg.requirejs.paths
          map:     cfg.requirejs.map
          shim:    cfg.requirejs.shim
          name:    'js/contents',
          out:     'built/contents.js'

    # compile sass to css
    sass:
      dist:
        files:
          'css/print.css': 'sass/print.scss'
          'css/meo.css': 'sass/meo.scss'

    lineending:
      dist:
        options:
          eol: 'crlf'
          overwrite: true
        files:
          '' : ['{coffee,css,dummy,js,sass,templates}/**/*.{coffee,css,html,js,json}', '*.{coffee,js,json}']

  grunt.registerTask 'build', ['requirejs', 'lineending']
  grunt.registerTask 'default', ['build']
