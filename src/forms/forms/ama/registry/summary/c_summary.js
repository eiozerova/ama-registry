define([
	  'forms/base/c_binded'
	, 'tpl!forms/ama/registry/summary/e_summary.html'
],
function (c_binded, tpl)
{
	return function()
	{
		function number_format(number, decimals, dec_point, separator)
		{
			number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
			var n = !isFinite(+number) ? 0 : +number,
			  prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
			  sep = (typeof separator === 'undefined') ? ',' : separator,
			  dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
			  s = '',
			  toFixedFix = function (n, prec)
			  {
			  	var k = Math.pow(10, prec);
			  	return '' + (Math.round(n * k) / k)
				  .toFixed(prec);
			  };
			// Фиксим баг в IE parseFloat(0.55).toFixed(0) = 0;
			s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
			  .split('.');
			if (s[0].length > 3)
			{
				s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
			}
			if ((s[1] || '')
			  .length < prec)
			{
				s[1] = s[1] || '';
				s[1] += new Array(prec - s[1].length + 1)
				  .join('0');
			}
			return s.join(dec);
		}

		var controller = c_binded(tpl, { foreign_data: function (n) { return number_format(n, 0, ',', ' '); } });

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			var empty_queue = { "Кредиторов": '', "Требований": '', "На_сумму": '' };
			this.model = {
				"1-я очередь": empty_queue
				, "2-я очередь": empty_queue
				, "3-я очередь": {
					"без залога": empty_queue
					, "с залогом": empty_queue
					, "пени": empty_queue
					, "штрафы": empty_queue
				}
				, "за реестром": {
					"пени": empty_queue
					, "штрафы": empty_queue
				}
			};

			base_Render.call(this, sel);
			this.binding.format_number = function (n) { return number_format(n, 0, ',', ' '); }

			var self = this;
			var sOnClick = function (e) { self.OnClick(e); };

			$.ajax
			({
				url: 'ama/registry/summary'
				, dataType: "json"
				, type: 'POST'
				, cache: false
				, error: function (data, textStatus) { alert('error of getting summary'); }
				, success: function (data, textStatus)
				{
					$(sel).html('');
					$(sel).html(tpl({ model: data, foreign_data: self.binding.foreign_data }));
					self.Rebind(sOnClick);
				}
			});

			this.Rebind(sOnClick);
		}

		controller.Rebind = function (sOnClick)
		{
			var self = this;
			var sel = this.binding.form_div_selector;
			var tr = $(sel + ' table tr');
			tr.off('click', sOnClick);
			tr.click(sOnClick);
			this.SetSelection(this.selection);
		}

		controller.selection = '3-я очередь$без залога';

		controller.OnClick= function(e)
		{
			e.preventDefault();
			this.SetSelection($(e.target).parent('tr').attr('selection'));
		}

		controller.SetSelection= function(selection)
		{
			var sel = this.binding.form_div_selector;
			$(sel + ' table tr').removeClass('selected');
			$(sel + ' table tr[selection="' + selection + '"]').addClass('selected');
			if (this.OnChangeSelection)
				this.OnChangeSelection(selection);
		}

		return controller;
	}
});
