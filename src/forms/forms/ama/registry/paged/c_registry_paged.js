define([
	  'forms/base/c_binded'
	, 'tpl!forms/ama/registry/paged/e_registry_paged.html'
],
function (c_binded, tpl)
{
	return function()
	{
		var colModel =
		[
			  { label: '№', name: 'Номер', width: 35, sortable: true, searchoptions: { sopt: ['cn'] }, align: 'right' }
			, { label: 'оч.', name: 'Очередь', width: 20, sortable: true, align: 'center', searchoptions: { sopt: ['cn'] } }
			, { label: 'вид об.', name: 'Вид', width: 50, sortable: true, searchoptions: { sopt: ['cn'] } }
			, { label: 'кредитор', name: 'Кредитор', sortable: true, searchoptions: { sopt: ['cn'] } }
			, { label: 'размер (руб.)', name: 'Сумма', width: 75, sortable: true, searchoptions: { sopt: ['cn'] }, align: 'right' }
			, { label: 'дата вкл.', name: 'Дата_включения', width: 55, sortable: true, align: 'center', search: false }
		];

		var controller = c_binded(tpl);

		controller.colModel = colModel;

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);
			this.RenderGrid();
		}

		controller.selection = '3-я очередь$без залога';
		controller.base_grid_url = 'ama/registry/items';
		controller.PrepareUrl= function()
		{
			return (''==this.selection)
				? (this.base_grid_url)
				: (this.base_grid_url + '/' + this.selection);
		}

		controller.RenderGrid = function ()
		{
			var self = this;
			var sel = this.binding.form_div_selector;
			var grid = $(sel + ' table.grid');
			grid.jqGrid
			({
				  datatype: "json"
				, url: self.PrepareUrl()
				, colModel: this.colModel
				, gridview: true
				, loadtext: 'Загрузка...'
				, recordtext: 'Требования {0} - {1} из {2}'
				, emptyText: 'Нет требований удовлетворяющим условиям фильтрации для просмотра'
				, rownumbers: false
				, rowNum: 10
				, rowList: [5, 10, 15]
				, pager: '#gridPager'
				, viewrecords: true
				, height: 'auto'
				, width: '600'
				, multiselect: true
				, multiboxonly: true
				, ignoreCase: true
			});
			grid.jqGrid('filterToolbar', { stringResult: true, searchOnEnter: false });
		}

		controller.ChangeSelection= function(selection)
		{
			this.selection= selection;
			if (!selection || ''==selection)
			{
				var title1 = 'Все требования';
				var title2 = '';
			}
			else
			{
				var parts = selection.split('$');
				if (1==parts.length)
				{
					var title1 = parts[0];
					var title2 = '';
				}
				else
				{
					var title1 = parts[0];
					var title2 = ' - ' + parts[1];
				}
			}
			var self = this;
			var sel = this.binding.form_div_selector;
			$(sel + ' div.ama-registry-paged span.title1').text(title1);
			$(sel + ' div.ama-registry-paged span.title2').text(title2);

			var grid = $(sel + ' table.grid');
			grid.setGridParam({ page: 1, url: self.PrepareUrl() }).trigger("reloadGrid");
		}

		return controller;
	}
});
