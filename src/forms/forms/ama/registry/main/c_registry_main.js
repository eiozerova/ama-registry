define([
	  'forms/base/c_binded'
	, 'tpl!forms/ama/registry/main/e_registry_main.html'
	, 'forms/ama/registry/paged/c_registry_paged'
	, 'forms/ama/registry/summary/c_summary'
],
function (c_binded, tpl, c_paged, c_summary)
{
	return function()
	{
		var controller = c_binded(tpl);

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);

			this.paged = c_paged();
			this.paged.CreateNew(sel + ' div.ama-registry-main div.grid');

			this.summary = c_summary();
			this.summary.CreateNew(sel + ' div.ama-registry-main div.selector');

			var self = this;
			this.summary.OnChangeSelection = function (selection) { self.paged.ChangeSelection(selection); }

			$(sel + ' div.commands button.change').button().click(function (e) { e.preventDefault(); self.OnChange(); });
			$(sel + ' div.commands button.open').button().click(function (e) { e.preventDefault(); self.OnOpen(); });
			$(sel + ' div.commands button.print').button().click(function (e) { e.preventDefault(); self.OnPrint(); });
			$(sel + ' div.commands button.extra').button();
		}

		controller.OnChange= function()
		{
			alert('OnChange');
		}

		controller.OnOpen = function ()
		{
			alert('OnOpen');
		}

		controller.OnPrint = function ()
		{
			alert('OnPrint');
		}

		return controller;
	}
});
