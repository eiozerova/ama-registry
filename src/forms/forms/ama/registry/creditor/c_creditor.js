define([
        'forms/base/c_binded', 'tpl!forms/ama/registry/creditor/e_creditor.html'
    ],
    function(c_binded, tpl) {
        return function() {
            var controller = c_binded(tpl);

            var base_Render = controller.Render;
            controller.Render = function(sel) {
                base_Render.call(this, sel);

                var self = this;

                var person_type_select = $(sel + ' select.person-type');
                person_type_select.change(function(e) {
                    self.SetPersonType(person_type_select.val());
                });
                self.SetPersonType(person_type_select.val());

                var cbox_structured_bank_data = $(sel + ' input.structured-bank-data');
                cbox_structured_bank_data.change(function(e) {
                    self.SetStructuredBankData(cbox_structured_bank_data.attr('checked'));
                });
                self.SetStructuredBankData(cbox_structured_bank_data.attr('checked'));

                this.LookupRender();
            }

            var base_GetFormContent = controller.GetFormContent;
            controller.GetFormContent = function() {
                var res = base_GetFormContent.call(this);
                switch (res.Тип) {
                case 'legal':
                    delete res.Фамилия;
                    delete res.Имя;
                    delete res.Отчество;
                    delete res.Паспорт;
                    break;
                case 'natural':
                    delete res.Наименование;
                    delete res.Руководитель;
                    delete res.Адрес.Фактический;
                    break;
                }
                if (true == res.Банковские_атрибуты.Структурировано || 'true' == res.Банковские_атрибуты.Структурировано) {
                    delete res.Банковские_атрибуты.текст;
                } else {
                    delete res.Банковские_атрибуты.Банк;
                    delete res.Банковские_атрибуты.Расчётный_счёт;
                }
                return res;
            }

            controller.LookupRender = function() {
                //Данную функцию рендера вызывай по загрузке формы

                var requestUrl = "https://probili.ru/rest/company?auth_token=7b944ac60e6b04df5324565f2bd5fece&query=%QUERY&limit=20&offset=0&options[category]=&options[region]=&options[city]=&options[street]=&options[ogrn]=&options[inn]=&options[okpo]=&options[bik]=&options[kpp]=&options[court]=";

                $.support.cors = true;
                var counterpartyProbili = new Bloodhound({
                    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('stitle'),
                    queryTokenizer: Bloodhound.tokenizers.whitespace,
                    remote: {
                        url: requestUrl,
                        wildcard: '%QUERY',
                        filter: function(counterparty) {
                            return $.map(counterparty.data.items, function(counterparty) {
                                return {
                                    title: counterparty.title,
                                    stitle: counterparty.stitle,
                                    fio: counterparty.fio,
                                    inn: counterparty.inn,
                                    ogrn: counterparty.ogrn,
                                    city: counterparty.city,
                                    addressf: counterparty.addressf,
                                    address: counterparty.address,
                                    id: "",
                                    region: counterparty.region
                                };
                            });
                        }
                    }
                });

                var counterpartyBase = global_external_AMA_GetContacts();

                var sel = this.binding.form_div_selector;
                counterpartyProbili.initialize();

                var my_Suggestion_class = new Bloodhound({
                    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('stitle'),
                    queryTokenizer: Bloodhound.tokenizers.whitespace,
                    local: $.map(counterpartyBase, function(item) {
                        return {
                            title: item.stitle,
                            stitle: item.stitle,
                            fio: item.fio,
                            inn: item.inn,
                            ogrn: item.ogrn,
                            city: item.city,
                            addressf: item.addressf,
                            address: item.address,
                            bank_req: item.bank_req,
                            id: item.id,
                            region: item.region
                        };
                    })
                });

                $(sel + ' .typeahead').typeahead({
                    limit: 10,
                    highlight: true
                },
                {
                    name: 'counterparty-base',
                    displayKey: 'stitle',
                    source: my_Suggestion_class.ttAdapter(),
                    templates: {
                        suggestion: Handlebars.compile("<div style='padding-left:6px'><b>{{stitle}}</b><br/>ИНН: {{inn}} ОГРН: {{ogrn}}<br/>{{city}}<br/><span style='float:right; color: #888;'><i>из раздела \"Контрагенты\"</i></span><br/></div>")
                    }
                },
                {
                    name: 'counterparty-probili',
                    displayKey: 'stitle',
                    source: counterpartyProbili.ttAdapter(),
                    templates: {
                        suggestion: Handlebars.compile("<div style='padding-left:6px'><b>{{stitle}}</b><br/>ИНН: {{inn}} ОГРН: {{ogrn}}<br/>{{city}}<br/><span style='float:right; color: #888;'><i>из probili.ru</i></span><br/></div>")
                    }
                });

                $(sel + ' .typeahead').bind('typeahead:select', function(ev, response_data) {
                    global_external_AMA_SetContactId(JSON.stringify(response_data));
                    controller.SetFormContent(JSON.stringify(getCreditorModel(response_data)));
                    controller.Edit('body div.cpw-ama-creditor');
                });
            }

            function getCreditorModel(data) {
                var lastname = "";
                var firstname = "";
                var middlename = "";
                var text = data.fio;
                var bankReq;
                if (data.bank_req == null || data.bank_req == undefined) {
                    bankReq = {
                        Структурировано: true,
                        Банк: {
                            Наименование: "",
                            Отделение: "",
                            БИК: "",
                            Корреспондентский_счёт: ""
                        },
                        Расчётный_счёт: ""
                    }
                } else {
                    if (data.bank_req.is_structured) {
                        bankReq = {
                            Структурировано: true,
                            Банк: {
                                Наименование: data.bank_req.bank.name,
                                Отделение: data.bank_req.bank.branch_name,
                                БИК: data.bank_req.bank.bik,
                                Корреспондентский_счёт: data.bank_req.bank.korrschet
                            },
                            Расчётный_счёт: data.bank_req.schet
                        }
                    } else {
                        bankReq = {
                            Структурировано: false,
                            Текст: data.bank_req.text
                        }
                    }
                }

                if (text !== null && text !== undefined && text !== "") {
                    var fio = text.split(/\s/, 3);
                    if (fio.length > 0) {
                        lastname = fio[0];
                    }
                    if (fio.length > 1) {
                        firstname = fio[1];
                    }
                    if (fio.length > 2) {
                        middlename = fio[2];
                    }
                }
                return {
                    Наименование: data.stitle,
                    Руководитель: {
                        Фамилия: lastname,
                        Имя: firstname,
                        Отчество: middlename
                    },
                    Контактные_телефоны: "",
                    Банковские_атрибуты: bankReq,
                    Тип: "legal",
                    Адрес: {
                        Фактический: data.addressf,
                        Почтовый: data.address
                    }
                };
            }

            controller.SetPersonType = function(person_type) {
                var sel = this.binding.form_div_selector;
                var div = $(sel + ' div.ama-creditor');
                div.removeClass('natural');
                div.removeClass('legal');
                div.addClass(person_type);
            }

            controller.SetStructuredBankData = function(checked) {
                var sel = this.binding.form_div_selector;
                var div = $(sel + ' div.ama-creditor');
                if ('checked' == checked) {
                    div.removeClass('unstructured-bank-data');
                    div.addClass('structured-bank-data');
                } else {
                    div.addClass('unstructured-bank-data');
                    div.removeClass('structured-bank-data');
                }
            }

            return controller;
        }
    });
