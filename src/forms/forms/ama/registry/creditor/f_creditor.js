define(['forms/ama/registry/creditor/c_creditor'],
function (CreateController)
{
	var form_spec =
	{
		  CreateController: CreateController
		, key: 'creditor'
		, Title: 'Информация о кредиторе'
		, FileFormat: 
			{
				  FileExtension: 'cri'
				, Description: 'CRI. Информация о кредиторе'
			}
	};
	return form_spec;
});
