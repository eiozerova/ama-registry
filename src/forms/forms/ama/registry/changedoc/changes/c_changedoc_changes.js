define([
	  'forms/base/c_binded'
	, 'tpl!forms/ama/registry/changedoc/changes/e_changedoc_changes.html'
	, 'forms/base/b_collection'
	, 'forms/ama/registry/changedoc/changes/change/include/main/c_include_main'
],
function (c_binded, tpl, b_collection, c_include_main)
{
	return function()
	{
		var controller = c_binded(tpl, 
			{ 
				CreateBinding: b_collection
				,Marker_class: 'changes'
				,BuildControllerForItem: function (change)
				{
					console.log('BuildControllerForItem');
					return c_include_main();
				}
			});

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);

			var on_add = function (e) { e.preventDefault(); self.OnAdd($(e.target).attr('cmd')); }

			var self = this;
			$(sel + ' div.commands button.add').button();
			$(sel + ' div.commands li').click(on_add);
			var btn_add = $(sel + ' div.commands button.add');
			btn_add.button({ icons: { primary: "ui-icon-plus" } }).click(on_add)
				.next().button({ text: false, icons: { primary: "ui-icon-triangle-1-s" } })
				.click(function ()
				{
					var menu = $(this).parent().next().show().position
					({
						my: "right top",
						at: "right bottom",
						of: this
					});
					$(document).one("click", function () { menu.hide(); });
					return false;
				})
				.parent().buttonset()
				.next().hide().menu()
			;

			this.on_delete = function (e) { self.OnDelete(e); };

			this.binding.on_after_update_dom = function () { self.OnAfterUpdateDom(); }
			this.OnAfterUpdateDom();

			if (!this.model || null == this.model || 0 == this.model.length)
				btn_add.click();
		}

		var base_GetFormContent = controller.GetFormContent;
		controller.GetFormContent = function ()
		{
			console.log('GetFormContent {');
			var res = base_GetFormContent.call(this);
			var controllers= this.binding.controllers_for_items;
			if (controllers)
			{
				console.log('GetFormContent read controllers..');
				for (var i = 0; i < res.length && i<controllers.length; i++)
				{
					console.log('GetFormContent read controller ' + i);
					var controller= controllers[i];
					if (null != controller)
					{
						console.log('GetFormContent read controller GetFormContent');
						res[i] = controller.GetFormContent();
						console.log(res[i]);
					}
				}
			}
			console.log('GetFormContent }');
			return res;
		}

		controller.OnAfterUpdateDom= function()
		{
			var self = this;
			var sel = this.binding.form_div_selector;

			$(sel + ' div.change-head button.delete').each(function ()
			{
				var bitem = $(this);
				
				if (!bitem.hasClass('ui-button'))
					bitem.button({ icons: { primary: "ui-icon-trash" } });
				bitem.off('click', self.on_delete);
				bitem.click(self.on_delete);
			});

			$(sel + ' div.change-head div.change-body').each(function ()
			{
				var ditem = $(this);
				if (ditem.hasClass('empty'))
				{
					var i_item = parseInt(obj.find_binding_for_item($(ditem)).i_item);
					var item = this.model[i_item];

					ditem.removeClass('empty')
				}
			});
		}

		controller.OnAdd= function(cmd)
		{
			var change = { cmd: cmd };
			switch (cmd)
			{
				case "Включить": change.Заголовок= 'Включить требование кредитора'; break;
				case "Внести_списком": change.Заголовок= 'Внести требования списком (импорт)'; break;
				case "Исключить": change.Заголовок= 'Исключить требование кредитора'; break;
				case "Перевести_в_необеспеченные": change.Заголовок= 'Перевести требование в НЕ обеспеченные залогом'; break;
				case "Перевести_в_обеспеченные": change.Заголовок= 'Перевести требование в обеспеченные залогом'; break;
				case "Начислить_проценты": change.Заголовок= 'Начислить проценты'; break;
				case "Зарегистрировать_погашение": change.Заголовок= 'Зарегистрировать погашение'; break;
				default:
					alert('Неизвестная команда: ' + cmd);
					return;
			}

			if (!this.model)
				this.model = this.binding.model = [];

			this.model.push(change);
			this.binding.update_dom();

			var sel = this.binding.form_div_selector;
			var change_controller = this.binding.options.BuildControllerForItem(change);
			console.log('change_controller.Edit ----------------------------------- {');
			change_sel = sel + ' div.changes.not-empty-list div.changes.collection-item[collection-index="' + (this.model.length - 1) + '"] div.collection-item-control';
			console.log('change_sel:' + change_sel);
			change_controller.Edit(change_sel);
			console.log('change_controller.Edit ----------------------------------- }');

			if (!this.binding.controllers_for_items)
			{
				this.binding.controllers_for_items = [change_controller];
			}
			else
			{
				this.binding.controllers_for_items.push(change_controller);
			}
		}

		controller.OnDelete= function(e)
		{
			e.preventDefault();
			var obj = this.binding;
			var i_item = parseInt(obj.find_binding_for_item($(e.target)).i_item);
			var change = obj.model[i_item];
			if (confirm('Вы действительно хотите удалить измерение № ' + (i_item + 1) + '\r\n\r\n\t"' + change.Заголовок + '"?'))
			{
				obj.model.splice(i_item, 1);
				obj.update_dom();
			}
		}

		return controller;
	}
});
