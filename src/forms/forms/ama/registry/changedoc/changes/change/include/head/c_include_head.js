define([
	  'forms/base/c_binded'
	, 'tpl!forms/ama/registry/changedoc/changes/change/include/head/e_include_head.html'
],
function (c_binded, tpl)
{
	return function ()
	{
		console.log('create c_include_head {');
		var controller = c_binded(tpl);

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			console.log('c_include_head.Render {');
			base_Render.call(this, sel);
			console.log('c_include_head.Render }');
			var self = this;

			$(sel + ' button.creditor-params').button();
		}
		console.log('create c_include_head }');
		return controller;
	}
});
