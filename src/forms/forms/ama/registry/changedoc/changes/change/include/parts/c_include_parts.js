define([
	  'forms/base/c_binded'
	, 'tpl!forms/ama/registry/changedoc/changes/change/include/parts/e_include_parts.html'
	, 'forms/base/b_collection'
	, 'tpl!forms/ama/registry/changedoc/changes/change/include/parts/v_include_parts_delete_confirm.html'
	, 'forms/base/h_msgbox'
],
function (c_binded, tpl, b_collection, v_include_parts_delete_confirm, h_msgbox)
{
	return function()
	{
		var controller = c_binded(tpl, 
			{ 
				CreateBinding: b_collection
				,Marker_class: 'include-parts'
			});

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			if (!this.model || null == this.model || 0 == this.model.length)
			{
				this.model = [
					{ Вид: 'Основной долг', Очередь: '3' }
					, { Вид: 'Пени', Очередь: '3' }
					, { Вид: 'Штраф', Очередь: '3' }
				];
			}

			base_Render.call(this, sel);

			var self = this;
			this.on_delete = function (e) { self.OnDelete(e); };
			$(sel + ' button.delete').button({ icons: { primary: "ui-icon-trash" } });
			$(sel + ' select.add').change(function () { self.OnSelectAdd(); });

			this.binding.on_after_update_dom = function () { self.OnAfterUpdateDom(); }
			this.OnAfterUpdateDom();
		}

		controller.OnSelectAdd = function ()
		{
			var sel = this.binding.form_div_selector;
			var selector= $(sel + ' select.add');
			console.log('change ' + selector.val());
			var selected_value = selector.val();
			this.model.push({ Вид: selected_value });
			selector.val('');
			this.binding.update_dom();
			var input_sel = sel + ' .collection-item[collection-index="' + (this.model.length - 1) + '"] [model_field_name="Вид"]';
			console.log(input_sel);
			$(input_sel).val(selected_value);
		}

		controller.OnAfterUpdateDom = function ()
		{
			var self = this;
			var sel = this.binding.form_div_selector;

			$(sel + ' .collection-item button.delete').each(function ()
			{
				var bitem = $(this);

				if (!bitem.hasClass('ui-button'))
					bitem.button({ icons: { primary: "ui-icon-trash" } });
				bitem.off('click', self.on_delete);
				bitem.click(self.on_delete);
			});
		}

		controller.OnDelete= function(e)
		{
			var sel = this.binding.form_div_selector;
			var ipart = $(e.target).parents('.collection-item.include-parts');
			console.log(ipart);
			var obj = {
				Вид: ipart.find('[model_field_name="Вид"]').val()
				, Размер: {
					рублей: ipart.find('[model_field_name="Размер.рублей"]').val()
					, копеек: ipart.find('[model_field_name="Размер.копеек"]').val()
				}
			};
			var self = this;
			h_msgbox.ShowModal
				({
					html: v_include_parts_delete_confirm(obj)
					, title: 'Подтверждение удаления требования'
					, width: '600'
					, buttons: ['Да', 'Нет']
					, onclose: function (bname)
					{
						if ('Да' == bname)
						{
							self.model.splice(i_item, 1);
							var i_item = parseInt(ipart.attr('collection-index'));
							console.log('i_item=' + i_item);
							ipart.remove();
							for (var i= i_item; i<self.model.length; i++)
							{
								console.log(i);
								var sel_part = sel + ' .collection-item.include-parts[collection-index="' + (i + 1) + '"]';
								console.log(sel_part);
								$(sel_part).attr('collection-index', i);
							}
						}
					}
				});
		}

		return controller;
	}
});
