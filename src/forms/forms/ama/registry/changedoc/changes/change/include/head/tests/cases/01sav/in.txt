include ..\..\..\..\..\..\..\..\..\..\wbt.lib.txt quiet
include ..\in.lib.txt quiet

execute_javascript_stored_lines add_wbt_std_functions

wait_text "Кредитор:"

shot_check_png ..\..\shots\0new.png

play_stored_lines changedoc_include_head_fields_1

shot_check_png ..\..\shots\01sav.png

wait_click_text "Сохранить содержимое формы"
dump_js wbt_controller_GetFormContentTextArea ..\..\contents\01sav.json.result.txt

exit