define([
	  'forms/base/c_binded'
	, 'tpl!forms/ama/registry/changedoc/changes/change/include/main/e_include_main.html'
	, 'forms/ama/registry/changedoc/changes/change/include/head/c_include_head'
	, 'forms/ama/registry/changedoc/changes/change/include/parts/c_include_parts'
],
function (c_binded, tpl, c_include_head, c_include_parts)
{
	return function()
	{
		console.log('create c_include_main {');
		var controller = c_binded(tpl, {
			field_spec: {
				В_том_числе: { controller: c_include_parts }
				, Заголовок: { controller: c_include_head }
			}
		});

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			console.log('c_include_main.Render {');
			console.log('sel=' + sel);
			base_Render.call(this, sel);
			console.log('c_include_main.Render }');
		}
		console.log('create c_include_main }');
		return controller;
	}
});
