{
	"Кредитор": {
		"Наименование": "ООО Рога и копыта"
	},
	"В_том_числе": [
		{
			"Вид": "счёт на счёт счёта",
			"Очередь": "1",
			"Размер": {
				"рублей": "100000",
				"копеек": "5"
			},
			"С_залогом": true
		},
		{
			"Вид": "Пени",
			"Очередь": "2",
			"Размер": {
				"рублей": "333333",
				"копеек": "00"
			},
			"С_залогом": false
		},
		{
			"Вид": "Штраф",
			"Очередь": "3",
			"Размер": {
				"рублей": "234544",
				"копеек": "07"
			},
			"С_залогом": false
		}
	]
}