define([
	  'forms/base/c_binded'
	, 'tpl!forms/ama/registry/changedoc/head/e_changedoc_head.html'
],
function (c_binded, tpl)
{
	return function()
	{
		var controller = c_binded(tpl);

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			console.log('c_changedoc_head.Render {');
			console.log('sel:' + sel)
			base_Render.call(this, sel);

			var self = this;
			$(sel + ' button.generate').button().click(function (e) { e.preventDefault(); self.OnGenerate(); });
			$(sel + ' button.open-file').button().click(function (e) { e.preventDefault(); self.OnOpenFile(); });
			console.log('c_changedoc_head.Render }');
		}

		controller.OnGenerate= function()
		{
			alert('OnGenerate');
		}

		controller.OnOpenFile= function ()
		{
			alert('OnOpenFile');
		}

		return controller;
	}
});
