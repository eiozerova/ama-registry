include ..\..\..\..\..\..\..\wbt.lib.txt quiet
include ..\in.lib.txt quiet

execute_javascript_stored_lines add_wbt_std_functions

wait_text "Документ:"

shot_check_png ..\..\shots\01sav.png

check_stored_lines changedoc_head_fields_1

play_stored_lines changedoc_head_fields_2

shot_check_png ..\..\shots\02edt.png

wait_click_text "Сохранить содержимое формы"
dump_js wbt_controller_GetFormContentTextArea ..\..\contents\02edt.json.result.txt

exit