define([
	  'forms/base/c_binded'
	, 'tpl!forms/ama/registry/changedoc/main/e_changedoc_main.html'
	, 'forms/ama/registry/changedoc/head/c_changedoc_head'
	, 'forms/ama/registry/changedoc/changes/c_changedoc_changes'
],
function (c_binded, tpl, c_changedoc_head, c_changedoc_changes)
{
	return function()
	{
		var controller = c_binded(tpl, {
			field_spec: {
				  Изменения: { controller: c_changedoc_changes }
				, ЗаголовокИзменяющегоДокумента: { controller: c_changedoc_head }
			}
		});

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);
		}

		return controller;
	}
});
