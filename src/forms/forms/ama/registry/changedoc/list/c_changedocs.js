define([
	  'forms/base/c_binded'
	, 'tpl!forms/ama/registry/changedoc/list/e_changedocs.html'
	, 'tpl!forms/ama/registry/changedoc/list/v_changedocs_group_confirm.html'
	, 'forms/base/h_msgbox'
	, 'forms/ama/registry/changedoc/main/c_changedoc_main'
],
function (c_binded, tpl, tpl_changedocs_group_confirm, h_msgbox, c_changedoc)
{
	return function()
	{
		var colModel =
		[
			  { label: 'документ', name: 'Наименование', sortable: true, search: false }

			, { label: 'от', name: 'Дата', width: 55, sortable: true, align: 'center', search: false }
			, { label: 'зарег.', name: 'ДатаРегистрации', width: 55, sortable: true, align: 'center', search: false }

			, {
				label: 'изм.', width: 40, sortable: true, align: 'center', search: false,
				formatter: function (value, options, row_data)
				{
					var res = '';
					res += (true === row_data.ВидыИзменения.Включения) ? '+' : '&nbsp';
					res += (true === row_data.ВидыИзменения.Изменения) ? '!' : '&nbsp';
					res += (true === row_data.ВидыИзменения.Исключения) ? '-' : '&nbsp';
					return res;
				}
			}

			, {
				label: 'статус', width: 40, sortable: true, align: 'center', search: false,
				formatter: function (value, options, row_data)
				{
					if (row_data.Заблокировано)
						return 'блокир.';
					if (row_data.Проверено)
						return 'провер.';
					return 'новый';
				}
			}

			, { label: 'сумма (руб.)', name: 'Сумма', width: 75, sortable: true, align: 'right', search: false }

			, { label: 'тр.', name: 'Требований', width: 40, sortable: true, align: 'right', search: false }
			, { label: 'кр.', name: 'Кредиторов', width: 40, sortable: true, align: 'right', search: false }
			, { label: 'оч.', name: 'Очередь', width: 40, sortable: true, align: 'center', search: false }

			, { label: 'пояснения', name: 'Пояснения', sortable: true, search: false }

			, { name: 'id', hidden: true, }
		];

		var controller = c_binded(tpl);

		controller.colModel = colModel;

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);
			this.RenderGrid();

			var self = this;
			$(sel + ' div.commands button.delete').button().click(function (e) { e.preventDefault(); self.OnDelete(); });
			$(sel + ' div.commands button.edit').button();
			$(sel + ' div.commands button.add').button().click(function (e) { e.preventDefault(); self.OnAdd(); });;

			this.RenderRTCButtons(sel);
			this.RenderStateButtons(sel);
			this.UpdateButtonsState();
		}

		controller.RenderRTCButtons = function (sel)
		{
			var on_rtc = function (e) { self.OnRTCCommand(e); };
			var self = this;
			$(sel + ' div.rtc-commands button.rtc').button();
			$(sel + ' div.rtc-commands li').click(on_rtc);
			var btn_rtc = $(sel + ' div.rtc-commands button.rtc');
			btn_rtc.button().click(on_rtc)
				.next().button({ text: false, icons: { primary: "ui-icon-triangle-1-s" } })
				.click(function ()
				{
					var menu = $(this).parent().next().show().position
					({
						my: "right top",
						at: "right bottom",
						of: this
					});
					$(document).one("click", function () { menu.hide(); });
					return false;
				})
				.parent().buttonset()
				.next().hide().menu()
			;
		}

		controller.RenderStateButtons = function (sel)
		{
			var on_state = function (e) { self.OnChangeState(e); };
			var self = this;
			$(sel + ' div.state-commands button.state').button();
			$(sel + ' div.state-commands li').click(on_state);
			var btn_state = $(sel + ' div.state-commands button.state');
			btn_state.button().click(on_state)
				.next().button({ text: false, icons: { primary: "ui-icon-triangle-1-s" } })
				.click(function ()
				{
					var menu = $(this).parent().next().show().position
					({
						my: "right top",
						at: "right bottom",
						of: this
					});
					$(document).one("click", function () { menu.hide(); });
					return false;
				})
				.parent().buttonset()
				.next().hide().menu()
			;
		}

		controller.base_grid_url = 'ama/registry/changedocs';
		controller.PrepareUrl= function()
		{
			return this.base_grid_url;
		}

		controller.RenderGrid = function ()
		{
			var self = this;
			var sel = this.binding.form_div_selector;
			var grid = $(sel + ' table.grid');
			grid.jqGrid
			({
				  datatype: "json"
				, url: self.PrepareUrl()
				, colModel: this.colModel
				, gridview: true
				, loadtext: 'Загрузка...'
				, recordtext: 'Документы {0} - {1} из {2}'
				, emptyText: 'Нет документов удовлетворяющим условиям фильтрации для просмотра'
				, rownumbers: false
				, rowNum: 10
				, rowList: [5, 10, 15]
				, pager: '#gridPager'
				, viewrecords: true
				, height: 'auto'
				, width: '1000'
				, multiselect: true
				, multiboxonly: true
				, ignoreCase: true
				, ondblClickRow: function () { self.OnEdit(); }
				, onSelectRow: function () { self.OnSelect(); }
				, onSelectAll: function () { self.OnSelect(); }
			});
			//grid.jqGrid('filterToolbar', { stringResult: true, searchOnEnter: false });
		}

		controller.OnSelect= function()
		{
			this.UpdateButtonsState();
		}

		controller.UpdateButtonsState = function ()
		{
			var sel = this.binding.form_div_selector;
			var selected = $(sel + " div.ama-changedocs table.grid").jqGrid('getGridParam', 'selarrrow').length;

			$(sel + ' button.edit').css('display', (1 == selected) ? 'inline-block' : 'none');
			$(sel + ' button.delete').css('display', (0 != selected) ? 'inline-block' : 'none');
			$(sel + ' div.state-commands').css('display', (0 != selected) ? 'inline-block' : 'none');

			var grid = $(sel + " div.ama-changedocs table.grid");
			console.log(grid.jqGrid('getGridParam', 'data'));
		}

		controller.OnDelete = function ()
		{
			this.GroupOperationWithConfirmation('Подтверждение удаления',
				function (changedocs) { return tpl_changedocs_group_confirm({ changedocs: changedocs, action: 'удалить' }); },
				'ama/registry/changedocs/delete');
		}

		controller.OnChangeState = function (e)
		{
			console.log('OnChangeState');
			e.preventDefault();
			var state = $(e.target).attr('cmd');
			console.log('OnChangeState');
			switch (state)
			{
				case 'проверено':
					this.GroupOperationWithConfirmation('Подтверждение проверки',
						function (changedocs) { return tpl_changedocs_group_confirm({ changedocs: changedocs, action: 'проверено' }); },
						'ama/registry/changedocs/confirm');
					break;
				case 'заблокировано':
					this.GroupOperationWithConfirmation('Подтверждение блокировки',
						function (changedocs) { return tpl_changedocs_group_confirm({ changedocs: changedocs, action: 'заблокировано' }); },
						'ama/registry/changedocs/block');
					break;
			}
		}

		controller.OnRTCCommand= function(e)
		{
			e.preventDefault();
			var cmd = $(e.target).attr('cmd');
			alert('Команда ' + cmd + ' пока не реализована!');
		}

		controller.GroupOperationWithConfirmation = function (ctitle, tpl_changedocs_do_confirm, ajax_url)
		{
			var self = this;
			var sel = this.binding.form_div_selector;
			var grid = $(sel + " div.ama-changedocs table.grid");
			var selected_row_ids = grid.jqGrid('getGridParam', 'selarrrow');

			if (0 != selected_row_ids.length)
			{
				var docs_to_do = [];
				for (var i = 0; i < selected_row_ids.length; i++)
				{
					var doc_to_do = grid.jqGrid('getRowData', selected_row_ids[i]);
					docs_to_do.push(doc_to_do);
				}
				h_msgbox.ShowModal
				({
					html: tpl_changedocs_do_confirm(docs_to_do)
					, title: ctitle
					, width: '600'
					, buttons: ['Да', 'Нет']
					, onclose: function (bname)
					{
						if ('Да' == bname)
						{
							var id_to_do = [];
							for (var i = 0; i < docs_to_do.length; i++)
							{
								id_to_do.push(docs_to_do[i].id);
							}
							$.ajax
							({
								url: ajax_url
								, dataType: "json"
								, data: { id_to_do: id_to_do }
								, type: 'POST'
								, cache: false
								, error: function (data, textStatus) { alert('error of ' + ajax_url); }
								, success: function (data, textStatus)
								{
									grid.trigger("reloadGrid");
								}
							});
						}
					}
				});
			}
		}

		controller.OnAdd = function ()
		{
			var obj = this.binding;
			var editor = c_changedoc();

			var SaveButtonName = 'Сохранить';

			editor.SetFormContent({});

			var self = this;
			h_msgbox.ShowModal
				({
					controller: editor
					, title: 'Добавление изменяющего РТК документа'
					, width: 1010
					, height: 600
					, id_div: 'cpw-ama-registry-modal-changeset'
					, buttons: [SaveButtonName, 'Отмена']
					, onclose: function (bname, dlg)
					{
						if (SaveButtonName == bname)
						{
							/*h_validation_msg.IfOkWithValidateResult(editor.Validate(), function ()
							{
								var lot = editor.GetFormContent();
								if (!obj.model || null == obj.model)
								{
									obj.model = [lot];
								}
								else
								{
									obj.model.push(lot);
								}
								obj.update_dom();
								$(dlg).dialog('close');
							});
							return false;*/
						}
					}
				});
		}

		return controller;
	}
});
