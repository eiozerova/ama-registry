define
(
	[
		  'contents/collector'
		, 'txt!forms/ama/registry/summary/tests/contents/empty.json.txt'
		, 'txt!forms/ama/registry/summary/tests/contents/test1.json.txt'

		, 'txt!forms/ama/registry/changedoc/head/tests/contents/01sav.json.etalon.txt'

		, 'txt!forms/ama/registry/changedoc/changes/change/include/head/tests/contents/01sav.json.etalon.txt'

		, 'txt!forms/ama/registry/changedoc/changes/change/include/parts/tests/contents/test1.json.txt'
		, 'txt!forms/ama/registry/changedoc/changes/change/include/parts/tests/contents/01sav.json.etalon.txt'
		, 'txt!forms/ama/registry/changedoc/changes/change/include/parts/tests/contents/01sav_del.json.etalon.txt'

		, 'txt!forms/ama/registry/creditor/tests/contents/01sav.json.etalon.txt'
		, 'txt!forms/ama/registry/creditor/tests/contents/02sav.json.etalon.txt'

		, 'txt!forms/ama/registry/changedoc/changes/change/include/main/tests/contents/01sav.json.etalon.txt'

		, 'txt!forms/ama/address/form/tests/contents/test1.json.txt'
	],
	function (collect)
	{
		return collect([
		  'rsummary-empty'
		, 'rsummary-test1'

		, 'rchangedoc-head1'

		, 'rchange-include-head1'

		, 'rchange-include-parts-test1'
		, 'rchange-include-parts-01sav'
		, 'rchange-include-parts-01sav-del'

		, 'creditor-01sav'
		, 'creditor-02sav'

		, 'rchange-include-01sav'

		, 'addr_test1'
		], Array.prototype.slice.call(arguments, 1));
	}
);