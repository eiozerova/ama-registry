﻿define([
	  'forms/base/c_binded'
	, 'tpl!forms/ama/address/test/e_address_test.html'
	, 'forms/ama/address/input/c_address_input'
	, 'forms/ama/address/textarea/c_address_textarea'
],
	function (c_binded, tpl, c_address_input, c_address_textarea)
{
	return function ()
	{
		var options =
		{
			field_spec:
			{
				  'address1': { controller: c_address_input }
				, 'address2': { controller: c_address_textarea }
				, 'address3': { controller: c_address_input }
				, 'address4': { controller: c_address_textarea }
			}
		};

		var controller = c_binded(tpl, options);

		return controller;
	}
});