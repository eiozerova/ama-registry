﻿define([
	'forms/base/c_binded'
	, 'tpl!forms/ama/address/form/e_address_form.html'
	, 'tpl!forms/ama/address/form/v_confirm_change_addr_parts.html'
	, 'tpl!forms/ama/address/form/v_confirm_change_addr_text.html'
	, 'forms/base/h_msgbox'

],
	function (c_binded, tpl, tpl_confirm_to_change_addr_parts, tpl_confirm_change_addr_text, h_msgbox)
	{
		return function ()
		{
			var controller = c_binded(tpl);
			var base_Render = controller.Render;
			controller.Render = function (sel)
			{
				base_Render.call(this, sel);
				var self = this;

				this.YandexGeoCode(function (geocoded)
				{
					self.geocoded = geocoded;
					var addr = self.GetAddressFieldsFromYandexAddress(geocoded);
					self.LoadSelectOptionsByAddressText(geocoded, addr);
				})
				this.TakeAdressFromTextarea();
				var номер_квартиры = self.ВыкуситьКвартируИзТекстаАдресом();
				$(sel + ' select').on('click', 'option', function () { self.OnSelectYandexVariant($(this), self.geocoded); });
				$(sel + ' button').addClass('normal_button')
				$(sel + ' textarea[model_field_name="Текстом"]').keydown(function ()
				{
					$(sel + ' button.refreshYandex').addClass('Attention_button');
					$(sel + ' ul, span.mini').removeClass('Attention_text');
					$(sel + ' div.mtitle_1, div.results').addClass('transparetElement');
					$(sel + ' span.conformity').html('');
				});
				$(sel + ' button.refreshYandex').click(function () { self.refreshYandexButton(self.geocoded); });
				$(sel + ' button.refreshTabParts').click(function () { self.refreshTabPartsButton(номер_квартиры) });
				self.не_стоит_менять_значения_в_инпутах();
			}

			controller.refreshTabPartsButton = function () 
			{
				var sel = this.binding.form_div_selector;
				var self = this;
				$(sel + ' span.mini, ul').removeClass('Attention_text');
				$(sel + ' button.refreshYandex').removeClass('Attention_button')
				$(sel + ' div.mtitle_1, div.results').addClass('transparetElement');
				self.AddHomeFromTabs();
				self.TakeAdressFromTextarea();
			}

			controller.refreshYandexButton = function ()
			{
				var sel = this.binding.form_div_selector;
				var self = this;
				$(sel + ' div.mtitle_1, div.results, span.ifOneResult').removeClass('transparetElement');
				$(sel + ' button').removeClass('Attention_button').addClass('normal_button')
				$(sel + ' ul, span.mini').addClass('Attention_text');
				self.YandexGeoCode(function (geocoded)
				{
					$(sel + ' select[class="selCity"] > :not(.delete)').remove();
					var addr = self.GetAddressFieldsFromYandexAddress(geocoded);
					self.LoadSelectOptionsByAddressText(geocoded, addr);
					$(sel + ' select').on('click', 'option', function () { self.OnSelectYandexVariant($(this), geocoded); });
					self.ВыкуситьКвартируИзТекстаАдресом();
					self.TakeAdressFromTextarea();
				});
			}

			controller.OnSelectYandexVariant = function (option, geocoded)
			{
				var sel = this.binding.form_div_selector;
				var self = this;
				$(sel + ' option[val="00"]').remove();
				var ivariant = parseInt(option.val());
				var номер_квартиры = self.ВыкуситьКвартируИзТекстаАдресом();
				var addr = self.GetAddressFieldsFromYandexAddress(geocoded, ivariant, номер_квартиры);
				var SaveButtonName = 'Да, заменить';
				h_msgbox.ShowModal
					({
						html: tpl_confirm_to_change_addr_parts(addr)
						, title: 'Подтверждение заполнения частей адреса'
						, width: 650, height: 'auto'
						, buttons: [SaveButtonName, 'Нет, оставить как есть']
						, onclose: function (bname, dlg)
						{
							if (SaveButtonName == bname)
							{
								self.DoChangeAddrParts(ivariant, geocoded, номер_квартиры);
							}
						}
					});
				return ivariant;
			}

			controller.DoChangeAddrParts = function (ivariant, geocoded, номер_квартиры)
			{
				var self = this;
				var sel = this.binding.form_div_selector;
				var addr = self.GetAddressFieldsFromYandexAddress(geocoded, ivariant, номер_квартиры);
				self.SetAddressFields(addr);
				this.TryChangeAddressTextByAddressParts(addr, ivariant);
				$(sel + ' span.mini, ul').removeClass('Attention_text');
				$(sel + ' button.refreshYandex').removeClass('Attention_button')

				$(sel + ' div.mtitle_1, div.results').addClass('transparetElement');
				$(sel + ' span.conformity')
					.html('Адрес текстом соответствует частям адреса представленным ниже.')
					.removeClass('Attention_text')
					.addClass('normal_word');
			}

			controller.TryChangeAddressTextByAddressParts = function (addr, ivariant)
			{
				var sel = this.binding.form_div_selector;
				var self = this;
				var SaveButtonName = 'Да, заменить';
				h_msgbox.ShowModal
					({
						html: tpl_confirm_change_addr_text(addr)
						, title: 'Подтверждение изменения адреса текстом'
						, id_div: 'cpw-addr-confirm-to-change-text'
						, width: 650, height: 'auto'
						, buttons: [SaveButtonName, 'Нет, оставить как есть']
						, onclose: function (bname, dlg)
						{
							if (SaveButtonName == bname)
							{
								self.DoChangeText(addr);
							}
						}
					});
			}

			controller.DoChangeText = function ()
			{
				var self = this;
				var sel = this.binding.form_div_selector;
				var номер_квартиры = self.ВыкуситьКвартируИзТекстаАдресом();
				var условие_номера_квартиры_целиком = !номер_квартиры ? '' : ', ' + номер_квартиры[0];
				$(sel + ' textarea[model_field_name="Текстом"]')
					.val($(sel + ' option:selected').text())
					.append(условие_номера_квартиры_целиком);
				self.TakeAdressFromTextarea();
			}

			controller.YandexGeoCode = function (обработать_обработку)
			{
				var sel = this.binding.form_div_selector;
				var txtVal = $(sel + ' textarea[model_field_name="Текстом"]').val();
				$.ajax({
					url: 'https://geocode-maps.yandex.ru/1.x/?format=json&geocode=' + txtVal,
					dataType: 'json',
					async: false,
					success: function (результат_запроса_к_яндексу_по_адресу_текстом)
					{
						обработать_обработку(результат_запроса_к_яндексу_по_адресу_текстом);
					},
					error: function (jqXHR, exception)
					{
						if (jqXHR.status === 0)
						{
							alert('Не удалось проверить данные, отсутствует подключение к интернету...');
						} else if (exception === 'timeout')
						{
							alert('Время ожидания ответа прошло');
						} else
						{
							alert('Неопределенная ошибка');
						}
					}
				});
				if (txtVal == "")
				{
					$(sel + ' span.mini, ul').removeClass('Attention_text'); 
					$(sel + ' button.refreshYandex').removeClass('Attention_button')

					$(sel + ' div.mtitle_1, div.results').addClass('transparetElement');
				}
			}

			controller.TakeAdressFromTextarea = function ()
			{
				var sel = this.binding.form_div_selector;
				var valueFromTextarea = $(sel + ' textarea[model_field_name="Текстом"]').val();
				$(sel + ' span.verifiAdress').html(valueFromTextarea);
			}

			controller.RegSearchApartment = /(КВАРТИРА|КВ-РА|КВ|КОМНАТА|КОМН|ОФИС|ОФ|ПАВИЛЬОН|ПАВ|П|ПОМЕЩЕНИЕ|ПОМЕЩ|ПОМ|РАБОЧИЙ\sУЧАСТОК|РАБ\.\sУЧ|РАБОЧИЙ\.УЧ|РАБОЧИЙ\.\sУЧ|РАБ\.\sУЧАСТОК|СКЛАД|СКЛ|СК|ТОРГОВЫЙ\sЗАЛ|ТОРГ\.\sЗАЛ|Т\.З|ТЗ|ЦЕХ|Ц)(?:\.)?(?:\s)?(\d{1,}([А-я]{1})?([\/\\]{1}\d{1,})?)/i;

			controller.ВыкуситьКвартируИзТекстаАдресом = function ()
			{
				var sel = this.binding.form_div_selector;
				var valueFromTextarea = $(sel + ' textarea[model_field_name="Текстом"]').val();
				var номер_квартиры = valueFromTextarea.match(this.RegSearchApartment);
				var ifHomeNumberNull = !номер_квартиры ? '' : номер_квартиры[0];
				$(sel + ' input[model_field_name="Квартира"]').val(ifHomeNumberNull);
				return номер_квартиры;
			}

			controller.AddHomeFromTabs = function ()
			{
				var sel = this.binding.form_div_selector;
				var arr = []
				var field_names = ['Индекс', 'Регион', 'Город', 'Населенный_пункт', 'Район', 'Улица', 'Дом', 'Корпус', 'Строение', 'Квартира'];
				for (var i = 0; i < field_names.length; i++)
				{
					var name = field_names[i];
					var allValue = $(sel + ' input[model_field_name="' + name + '"]').val();
					var verification = !allValue ? "" : allValue;
					arr.push(verification);
				}
				var result = arr.filter(function (e) { return e });
				$(sel + ' textarea[model_field_name="Текстом"]').val(result.join(", "))
			}

			controller.LoadSelectOptionsByAddressText = function (jsonParse, addr)
			{
				var sel = this.binding.form_div_selector;
				var self = this;
				var txtVal = $(sel + ' textarea[model_field_name="Текстом"]').val();
				var txtValWithoutApartment = txtVal.replace(this.RegSearchApartment, '');
				var проверка_на_пробелы = txtValWithoutApartment.replace(/(?:\s$)|(?:\,$)|(?:\.$)/, '');
				self.return_true_values(jsonParse, проверка_на_пробелы);
				if ($(sel + ' option').size() == 1)
				{
					self.SetAddressFields(addr);
					$(sel + ' span.conformity')
						.html('Адрес текстом соответствует частям адреса представленным ниже.')
						.removeClass('Attention_text')
						.addClass('normal_word');
					$(sel + ' span.mini')
						.html($(sel + ' option').length + ' вариант')
						.removeClass('Attention_text');;
					$(sel + ' span.ifOneResult').addClass('transparetElement');
				}
				else if ($(sel + ' option').size() <= 5)
				{
					$(sel + ' ul, span.mini').addClass('Attention_text');
					$(sel + ' span.mini').html($(sel + ' option').size() - '1' + ' вариантов');
				} else
				{
					$(sel + ' ul, span.mini').addClass('Attention_text');
					$(sel + ' span.mini').html('много вариантов');
				}
			};

			controller.way_of_address_from_Json = function (i, wayOfAddress)
			{
				var option_text = wayOfAddress.featureMember[i].GeoObject.metaDataProperty.GeocoderMetaData.Address.formatted;
				var Индекс = wayOfAddress.featureMember[i].GeoObject.metaDataProperty.GeocoderMetaData.Address.postal_code;
				var есть_ли_Индекс = !Индекс ? '' : Индекс + ", ";
				var Индекс_и_Адрес = есть_ли_Индекс + option_text;
				return Индекс_и_Адрес;
			}

			controller.return_true_values = function (jsonParse, проверка_на_пробелы)
			{
				var sel = this.binding.form_div_selector;
				var self = this;
				var wayOfAddress = jsonParse.response.GeoObjectCollection;
				for (var i = 0; i < wayOfAddress.featureMember.length; i++)
				{
					var Индекс_и_Адрес = self.way_of_address_from_Json(i, wayOfAddress);
					var проверка_2 = self.find_true_result(i, проверка_на_пробелы, Индекс_и_Адрес);
				}
				if ($(sel + ' select[class="selCity"]').val() == null)
				{
					self.return_default_values(jsonParse, проверка_на_пробелы);
					if ($(sel + ' option').size() > 1)
					{
						$(sel + ' select[class="selCity"]').prepend('<option selected val="00"></option>');
					}
				}
			};

			controller.return_default_values = function (jsonParse, проверка_на_пробелы)
			{
				var sel = this.binding.form_div_selector;
				var self = this;
				var wayOfAddress = jsonParse.response.GeoObjectCollection;
				for (var i = 0; i < wayOfAddress.featureMember.length; i++)
				{
					var Индекс_и_Адрес = self.way_of_address_from_Json(i, wayOfAddress);
					$(sel + ' select[class="selCity"]').append('<option value="' + i + '">' + Индекс_и_Адрес + '</option>');
				}
			}

			controller.find_true_result = function (i, проверка_на_пробелы, Индекс_и_Адрес)
			{
				var sel = this.binding.form_div_selector;
				var qq = проверка_на_пробелы.split(/(?:,| )+/);
				var ww = Индекс_и_Адрес.split(/(?:,| )+/);
				var regEks = new RegExp(qq, "gi");
				var проверка_2 = regEks.exec(ww);
				if (проверка_2 != null)  
				{
					$(sel + ' select[class="selCity"]').append('<option value="' + i + '">' + Индекс_и_Адрес + '</option>');
				}
				return проверка_2;
			}

			controller.GetAddressFieldsFromYandexAddress = function (результат_запроса_к_яндексу_по_адресу_текстом, ivariant, номер_квартиры)
			{
				var sel = this.binding.form_div_selector;
				var self = this;
				if (!ivariant)
					ivariant = 0;
				var addr = {};
				try
				{
					var shortWay = результат_запроса_к_яндексу_по_адресу_текстом.response.GeoObjectCollection
						.featureMember[ivariant].GeoObject.metaDataProperty.GeocoderMetaData;
					var Индекс = !shortWay.Address.postal_code ? '' : shortWay.Address.postal_code + ", ";
					var проверка = Индекс + shortWay.Address.formatted;
					var wayOfAddress = shortWay.AddressDetails.Country.AdministrativeArea;
					var условие_только_номера_кваритиры = !номер_квартиры ? '' : номер_квартиры[0];
					var квартира_с_запятой = !условие_только_номера_кваритиры ? '' : ', ' + условие_только_номера_кваритиры
					addr.Квартира = условие_только_номера_кваритиры;
					addr.Текстом = проверка + квартира_с_запятой
					addr.Регион = wayOfAddress.AdministrativeAreaName;
					self.условие_проверки_адреса(проверка, addr, wayOfAddress)
				}
				catch (e)
				{
					$(sel + ' span.conformity')
						.html('Введен некорректный/неполный адрес, поэтому Яндекс не смог определить адрес полностью.')
						.removeClass('normal_word')
						.addClass('Attention_text');
					self.очистить_инпуты_при_ошибке();
				}
				return addr;
			};

			controller.условие_проверки_адреса = function (проверка, addr, wayOfAddress)
			{
				var self = this;
				if (/(район)/i.test(проверка))
				{
					self.GetAddressFieldsFromYandexAddressForOtherCity(addr, wayOfAddress);
				}
				else if (/(москва)|(санкт-петербург)/i.test(проверка))
				{
					self.GetAddressFieldsFromYandexAddressForMoscow(addr, wayOfAddress);
				}
				else
				{
					self.GetAddressFieldsFromYandexAddressForOtherCity(addr, wayOfAddress);
				}
			}

			controller.village_or_town = function (addr, city, БратьРайон)
			{
				if (!(/(деревня)|(село)|(посёлок)|(починок)/i.test(city)))
				{
					addr.Город = city;
				}
				else
				{
					addr.Населенный_пункт = city;
					БратьРайон = true;
					return БратьРайон;
				}
			}

			controller.GetAddressFieldsFromYandexAddressForMoscow = function (addr, wayOfAddress)
			{
				var sel = this.binding.form_div_selector;
				var self = this;
				var city = wayOfAddress.Locality.LocalityName;
				self.village_or_town(addr, city);
				var Thoroughfare = wayOfAddress.Locality.Thoroughfare
				addr.Улица = Thoroughfare.ThoroughfareName;
				addr.Дом = Thoroughfare.Premise.PremiseNumber;
				addr.Индекс = Thoroughfare.Premise.PostalCode.PostalCodeNumber;
			}

			controller.GetAddressFieldsFromYandexAddressForOtherCity = function (addr, wayOfAddress)
			{
				var sel = this.binding.form_div_selector;
				var self = this;
				var city = wayOfAddress.SubAdministrativeArea.Locality.LocalityName;
				var БратьРайон = self.village_or_town(addr, city, БратьРайон);
					if (true == БратьРайон)
				addr.Район = wayOfAddress.SubAdministrativeArea.SubAdministrativeAreaName;
				var Thoroughfare = wayOfAddress.SubAdministrativeArea.Locality.Thoroughfare;
				addr.Улица = Thoroughfare.ThoroughfareName;
				addr.Дом = Thoroughfare.Premise.PremiseNumber;
				addr.Индекс = Thoroughfare.Premise.PostalCode.PostalCodeNumber;
			}


			controller.SetAddressFields = function (addr)
			{
				var sel = this.binding.form_div_selector;
				var field_names = ['Регион', 'Город', 'Населенный_пункт', 'Район', 'Улица', 'Дом', 'Индекс', 'Квартира'];
				for (var i = 0; i < field_names.length; i++)
				{
					var name = field_names[i];
					var value = !addr[name] ? '' : addr[name];
					$(sel + ' input[model_field_name="' + name + '"]').val(value);
				}
			}

			controller.AddressPartNames = ['Регион', 'Город', 'Населенный_пункт', 'Район', 'Улица', 'Дом', 'Индекс', 'Корпус', 'Строение', 'Квартира'];

			controller.не_стоит_менять_значения_в_инпутах = function ()
			{
				var sel = this.binding.form_div_selector;
				for (var i = 0; i < this.AddressPartNames.length; i++)
				{
					var name = this.AddressPartNames[i];
					$(sel + ' input[model_field_name="' + name + '"]').keydown(function ()
					{
						if ($(sel + ' input[model_field_name="' + name + '"]').val() != null)
						{
							$(sel + ' span.conformity')
								.html('Части адреса поменялись, будьте внимательны при заполнении адреса вручную.')
								.removeClass('normal_word')
								.addClass('Attention_text');
						}
					});
				}
			}

			controller.очистить_инпуты_при_ошибке = function ()
			{
				var sel = this.binding.form_div_selector;
				for (var i = 0; i < this.AddressPartNames.length; i++)
				{
					var name = this.AddressPartNames[i];
					$(sel + ' input[model_field_name="' + name + '"]').val('')
				}
			}
			return controller;
		}

	});