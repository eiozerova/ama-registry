﻿define([
	'forms/ama/address/form/c_address_form'
	, 'forms/base/h_msgbox'
	, 'forms/base/h_validation_msg'
],
	function (c_address_form, h_msgbox, h_validation_msg)
	{
		var helper = {
			ShowModal: function (data, on_ok)
			{
				var editor = c_address_form();
				editor.SetFormContent(data);

				var SaveButtonName = 'Сохранить параметры адреса';

				var self = this;
				h_msgbox.ShowModal
					({
						controller: editor
						, title: 'Форма ввода параметров адреса..'
						, width: 930
						, height: 670
						, id_div: 'cpw-ama-address-form-modal'
						, buttons: [SaveButtonName, 'Отмена']
						, onclose: function (bname, dlg)
						{
							if (SaveButtonName == bname)
							{
								h_validation_msg.IfOkWithValidateResult(editor.Validate(), function ()
								{
									on_ok(editor.GetFormContent());
									console.log(editor.GetFormContent())
									$(dlg).dialog('close');
								});
								return false;
							}
						}
					});
			}
		};

		return helper;

	});