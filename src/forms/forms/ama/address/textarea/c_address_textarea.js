﻿define([
	'forms/base/c_binded'
	, 'tpl!forms/ama/address/textarea/e_address_textarea.html'
	, 'forms/ama/address/form/h_address_form_modal'
],
	function (c_binded, tpl, h_address_form_modal)
	{
		return function ()
		{
			var controller = c_binded(tpl);

			var base_Render = controller.Render;
			controller.Render = function (sel)
			{
				base_Render.call(this, sel);

				var self = this;

				this.resizeInput();
				this.typeahead(function (результаты_поиска)
				{
					self.geocoded = результаты_поиска;
				});

				$(sel + ' textarea').blur(function () { self.AddressVerification(self.geocoded); });
				$(sel + ' textarea').focus(function () { $(sel + ' button.open').removeClass('error'); });

				$(sel + ' button.open').click(function () { self.OnOpen(); });
			}


			controller.typeahead = function (обработать_результаты)
			{
				var sel = this.binding.form_div_selector;
				$(sel + ' textarea').ready(function ()
				{
					$.support.cors = true;
					var url = "https://geocode-maps.yandex.ru/1.x/?format=json&geocode=%QUERY";
					var поисковик = new Bloodhound({
						datumTokenizer: Bloodhound.tokenizers.whitespace,
						queryTokenizer: Bloodhound.tokenizers.whitespace,
						remote: {
							url: url,
							wildcard: '%QUERY',
							filter: function (результаты_поиска)
							{
								обработать_результаты(результаты_поиска)
								return $.map(результаты_поиска.response.GeoObjectCollection.featureMember, function (результат_поиска)
								{
									var Адрес = результат_поиска.GeoObject.metaDataProperty.GeocoderMetaData.Address.formatted;
									var Индекс = результат_поиска.GeoObject.metaDataProperty.GeocoderMetaData.Address.postal_code;
									var есть_ли_Индекс = !Индекс ? '' : Индекс + ", ";
									return есть_ли_Индекс + Адрес;
								});
							}
						}
					});
					поисковик.initialize();
					$(sel + ' textarea').typeahead
						({
							hint: true,
							highlight: true,
							minLength: 1
						},
						{
							name: 'adress',
							source: поисковик.ttAdapter()
						});
				});
			}

			controller.AddressVerification = function (результаты_поиска)
			{
				var sel = this.binding.form_div_selector;
				var self = this;
				var addr = {};
				try
				{
					var shortWay = результаты_поиска.response.GeoObjectCollection.featureMember[0].GeoObject.metaDataProperty.GeocoderMetaData;
					var проверка = shortWay.Address.formatted;
					var wayOfAddress = shortWay.AddressDetails.Country.AdministrativeArea;

					addr.Регион = wayOfAddress.AdministrativeAreaName;
					if (/(район)/i.test(проверка))
					{
						self.GetAddressFieldsFromYandexAddressForOtherCity(addr, wayOfAddress);
					}
					else if (/(москва)|(санкт-петербург)/i.test(проверка))
					{
						self.GetAddressFieldsFromYandexAddressForMoscow(addr, wayOfAddress);
					}
					else
					{
						self.GetAddressFieldsFromYandexAddressForOtherCity(addr, wayOfAddress);
					}
					self.LoadSelectOptionsByAddressText(результаты_поиска);
				}
				catch (e)
				{
					$(sel + ' button.open').addClass('error');
				}
			};

			controller.GetAddressFieldsFromYandexAddressForMoscow = function (addr, wayOfAddress)
			{
				var mainCity = wayOfAddress.Locality.LocalityName
				if (!(/(деревня)|(село)|(посёлок)|(починок)/i.test(mainCity)))
				{
					addr.Город = mainCity;
				}
				else
				{
					addr.Населенный_пункт = mainCity;
				}
				var Thoroughfare = wayOfAddress.Locality.Thoroughfare
				addr.Улица = Thoroughfare.ThoroughfareName;
				addr.Дом = Thoroughfare.Premise.PremiseNumber;
				addr.Индекс = Thoroughfare.Premise.PostalCode.PostalCodeNumber;
				if (addr.Индекс == null)
				{
					$(sel + ' button.open').addClass('error');
				}
			}

			controller.GetAddressFieldsFromYandexAddressForOtherCity = function (addr, wayOfAddress)
			{
				var sel = this.binding.form_div_selector;
				var self = this;
				var city = wayOfAddress.SubAdministrativeArea.Locality.LocalityName;
				if (!(/(деревня)|(село)|(посёлок)|(починок)/i.test(city)))
				{
					addr.Город = city;
				}
				else
				{
					addr.Населенный_пункт = city;
					addr.Район = wayOfAddress.SubAdministrativeArea.SubAdministrativeAreaName;
				}
				var Thoroughfare = wayOfAddress.SubAdministrativeArea.Locality.Thoroughfare;
				addr.Улица = Thoroughfare.ThoroughfareName;
				addr.Дом = Thoroughfare.Premise.PremiseNumber;
				addr.Индекс = Thoroughfare.Premise.PostalCode.PostalCodeNumber;
				if (addr.Индекс == null)
				{
					$(sel + ' button.open').addClass('error');
				}
			}

			controller.LoadSelectOptionsByAddressText = function (jsonParse)
			{
				var sel = this.binding.form_div_selector;
				var self = this;
				var txtVal = $(sel + ' .tt-input').val();
				var regApartment = /(?:[\s,])(КВАРТИРА|КВ-РА|КВ|КОМНАТА|КОМН|ОФИС|ОФ|ПАВИЛЬОН|ПАВ|П|ПОМЕЩЕНИЕ|ПОМЕЩ|ПОМ|РАБОЧИЙ\sУЧАСТОК|РАБ\.\sУЧ|РАБОЧИЙ\.УЧ|РАБОЧИЙ\.\sУЧ|РАБ\.\sУЧАСТОК|СКЛАД|СКЛ|СК|ТОРГОВЫЙ\sЗАЛ|ТОРГ\.\sЗАЛ|Т\.З|ТЗ|ЦЕХ|Ц)(?:\.)?(?:\s)?(\d{1,}([А-я]{1})?([\/\\]{1}\d{1,})?)/i;
				var txtValWithoutApartment = txtVal.replace(regApartment, '');
				var проверка_на_пробелы = txtValWithoutApartment.replace(/(?:\s$)|(?:\,$)|(?:\.$)/, '');
				self.return_true_values(jsonParse, проверка_на_пробелы);
			};

			controller.return_true_values = function (jsonParse, проверка_на_пробелы)
			{
				var sel = this.binding.form_div_selector;
				var self = this;
				var wayOfAddress = jsonParse.response.GeoObjectCollection;
				var option_text = wayOfAddress.featureMember[0].GeoObject.metaDataProperty.GeocoderMetaData.Address.formatted;
				var Индекс = wayOfAddress.featureMember[0].GeoObject.metaDataProperty.GeocoderMetaData.Address.postal_code;
				var есть_ли_Индекс = !Индекс ? '' : Индекс + ", ";
				var Индекс_и_Адрес = есть_ли_Индекс + option_text;
				var проверка_2 = self.find_true_result(проверка_на_пробелы, Индекс_и_Адрес);
				if (проверка_2 == null)
				{
					$(sel + ' button.open').addClass('error');
				}
			}

			controller.find_true_result = function (проверка_на_пробелы, Индекс_и_Адрес)
			{
				var sel = this.binding.form_div_selector;
				var qq = проверка_на_пробелы.split(/(?:,| )+/);
				var ww = Индекс_и_Адрес.split(/(?:,| )+/);
				var regEks = new RegExp(qq, "gi");
				var проверка_2 = regEks.exec(ww);
				if (проверка_2 != null)  
				{
					$(sel + ' button.open').removeClass('error');
				}
				return проверка_2;
			}

			controller.resizeInput = function ()
			{
				var sel = this.binding.form_div_selector;
				var self = this;
				var ширина_кнопки = 30;
				function ei(sel_to_typeahead)
				{
					$(sel_to_typeahead).each(function ()
					{
						var $this = $(this);
						$this.height(1).width(1);
						if ($this.is(sel + 'textarea'))
						{
							$this.css({ 'min-height': '', 'max-height': '', 'min-width': '', 'max-width': '' });
						}
						var additionalHeight =
							parseInt($this.css('border-top-width')) +
							parseInt($this.css('border-bottom-width')) +
							parseInt($this.css('margin-top')) +
							parseInt($this.css('margin-bottom')) +
							parseInt($this.css('padding-top')) +
							parseInt($this.css('padding-bottom')),
							parentHeight = $this.parent().parent().height(),
							countedHeight = parentHeight - additionalHeight;
						var additionalWidth =
							parseInt($this.css('border-left-width')) +
							parseInt($this.css('border-right-width')) +
							parseInt($this.css('margin-left')) +
							parseInt($this.css('margin-right')) +
							parseInt($this.css('padding-left')) +
							parseInt($this.css('padding-right')),
							parentWidth = $this.parent().parent().width(),
							countedWidth = parentWidth - additionalWidth - ширина_кнопки;
						self.вычисление_размера($this, countedHeight, countedWidth)
					});
				}
				var sel_to_typeahead = sel + ' textarea.typeahead';
				$(sel_to_typeahead).ready(function () { ei(sel_to_typeahead); });
			};

			controller.вычисление_размера = function ($this, countedHeight, countedWidth)
			{
				var sel = this.binding.form_div_selector;
				$this.height(countedHeight).width(countedWidth);
				if ($this.is(sel + 'textarea'))
				{
					$this.css({
						'min-height': countedHeight,
						'max-height': countedHeight,
						'min-width': countedWidth,
						'max-width': countedWidth
					});
				}
			}

			controller.OnOpen = function ()
			{
				this.model = this.GetFormContent();
				var self = this;
				h_address_form_modal.ShowModal(this.model, function (model) { self.model = model; self.Render(self.binding.form_div_selector); });
			}

			return controller;
		}
	}
);
