﻿include ..\..\..\..\..\..\wbt.lib.txt quiet
execute_javascript_stored_lines add_wbt_std_functions

wait_text "Сохранить содержимое формы"

shot_check_png ..\..\shots\01new.png

js wbt_SetValueBySpec(".tt-input[model_field_name='Текстом']", "Удмуртская Республика, Ижевск, Красногеройская улица, 109 кв 115");

je $(".tt-hint[model_field_name='Текстом']").blur();
                                
shot_check_png ..\..\shots\01sav_Izhevsk_Krasnogeroyskaya_109_invalid.png

je $("button.open").click();

wait_text "вариант"

shot_check_png ..\..\shots\01sav_Izhevsk_Krasnogeroyskaya_109_form.png

je $("option").val('0').click();

wait_text "Заменить части адреса на предложенные яндексом для выбранного варианта?"
shot_check_png ..\..\shots\01sav_Izhevsk_Krasnogeroyskaya_109_form_change_parts.png
click_text "Да, заменить"

wait_text "Заменить адреса текстом на предложенные выше вариант?"
shot_check_png ..\..\shots\01sav_Izhevsk_Krasnogeroyskaya_109_form_change_text.png
click_text "Да, заменить"

shot_check_png ..\..\shots\01sav_Izhevsk_Krasnogeroyskaya_109_form_not_ok.png

js wbt_SetValueBySpec("input[model_field_name='Индекс']", "426008");

click_text "Сформировать адрес текстом по отдельным частям адреса"

shot_check_png ..\..\shots\01sav_Izhevsk_Krasnogeroyskaya_109_form_ok.png

click_text "Сохранить параметры адреса"

shot_check_png ..\..\shots\01sav_Izhevsk_Krasnogeroyskaya_109_ok.png

wait_click_full_text "Сохранить содержимое формы"
dump_js wbt_controller_GetFormContentTextArea ..\..\contents\01sav_Izhevsk_Krasnogeroyskaya_109.json.result.txt

exit