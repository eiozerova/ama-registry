﻿include ..\..\..\..\..\..\wbt.lib.txt quiet
execute_javascript_stored_lines add_wbt_std_functions

wait_text "Сохранить содержимое формы"

shot_check_png ..\..\shots\07new.png

js wbt_SetValueBySpec(".tt-input[model_field_name='Текстом']", "427582, Удмуртская Республика, Кезский район, поселок городского типа Кез, улица Чайковского, дом 7");

je $(".tt-hint[model_field_name='Текстом']").blur();
                                
shot_check_png ..\..\shots\07sav_Kez_Chaykovskovo_7_invalid.png

je $("button.open").click();

wait_text "вариант"

shot_check_png ..\..\shots\07sav_Kez_Chaykovskovo_7_form.png

je $("option").val('1').click();

wait_text "Заменить части адреса на предложенные яндексом для выбранного варианта?"
shot_check_png ..\..\shots\07sav_Kez_Chaykovskovo_7_form_change_parts.png
click_text "Да, заменить"

wait_text "Заменить адреса текстом на предложенные выше вариант?"
shot_check_png ..\..\shots\07sav_Kez_Chaykovskovo_7_form_change_text.png
click_text "Да, заменить"

shot_check_png ..\..\shots\07sav_Kez_Chaykovskovo_7_form_ok.png

js wbt_SetValueBySpec("input[model_field_name='Дом']", "");

js wbt_SetValueBySpec("input[model_field_name='Дом']", "дом 7");

shot_check_png ..\..\shots\07sav_Kez_Chaykovskovo_7_parts_compile.png

click_text "Сформировать адрес текстом по отдельным частям адреса"

shot_check_png ..\..\shots\07sav_Kez_Chaykovskovo_7_parts_compile_ok.png

click_text "Сохранить параметры адреса"

shot_check_png ..\..\shots\07sav_Kez_Chaykovskovo_7_ok.png

wait_click_full_text "Сохранить содержимое формы"
dump_js wbt_controller_GetFormContentTextArea ..\..\contents\07sav_Kez_Chaykovskovo_7.json.result.txt

exit