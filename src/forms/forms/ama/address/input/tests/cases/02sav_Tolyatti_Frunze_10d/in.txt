﻿include ..\..\..\..\..\..\wbt.lib.txt quiet
execute_javascript_stored_lines add_wbt_std_functions

wait_text "Сохранить содержимое формы"

shot_check_png ..\..\shots\02new.png

js wbt_SetValueBySpec(".tt-input[model_field_name='Текстом']", "Россия, 445037, г. Тольятти, Самарская область, ул. Фрунзе, 10Д");

je $(".tt-hint[model_field_name='Текстом']").blur();
                                
shot_check_png ..\..\shots\02sav_Tolyatti_Frunze_10d_invalid.png

je $("button.open").click();

wait_text "вариант"

shot_check_png ..\..\shots\02sav_Tolyatti_Frunze_10d_form.png

je $("option").val('0').click();

wait_text "Заменить части адреса на предложенные яндексом для выбранного варианта?"
shot_check_png ..\..\shots\02sav_Tolyatti_Frunze_10d_form_change_parts.png
click_text "Да, заменить"

wait_text "Заменить адреса текстом на предложенные выше вариант?"
shot_check_png ..\..\shots\02sav_Tolyatti_Frunze_10d_form_change_text.png
click_text "Да, заменить"

shot_check_png ..\..\shots\02sav_Tolyatti_Frunze_10d_form_ok.png

click_text "Сохранить параметры адреса"

shot_check_png ..\..\shots\02sav_Tolyatti_Frunze_10d_ok.png

wait_click_full_text "Сохранить содержимое формы"
dump_js wbt_controller_GetFormContentTextArea ..\..\contents\02sav_Tolyatti_Frunze_10d.json.result.txt

exit