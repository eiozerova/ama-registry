define
(
	[
		  'forms/collector'
		, 'forms/ama/registry/summary/c_summary'
		, 'forms/ama/registry/paged/c_registry_paged'
		, 'forms/ama/registry/main/c_registry_main'
		, 'forms/ama/registry/changedoc/head/c_changedoc_head'
		, 'forms/ama/registry/changedoc/changes/c_changedoc_changes'
		, 'forms/ama/registry/changedoc/main/c_changedoc_main'
		, 'forms/ama/registry/changedoc/changes/change/include/head/c_include_head'
		, 'forms/ama/registry/changedoc/changes/change/include/main/c_include_main'
		, 'forms/ama/registry/changedoc/changes/change/include/parts/c_include_parts'
		, 'forms/ama/registry/changedoc/list/c_changedocs'
		, 'forms/ama/registry/creditor/c_creditor'
		, 'forms/ama/address/form/c_address_form'
		, 'forms/ama/address/input/c_address_input'
		, 'forms/ama/address/textarea/c_address_textarea'
		, 'forms/ama/address/test/c_address_test'
	],
	function (collect)
	{
		return collect([
		  'rsummary'
		, 'rpaged'
		, 'registry_main'
		, 'changedoc_head'
		, 'changedoc_changes'
		, 'changedoc'
		, 'include_head'
		, 'include'
		, 'include_parts'
		, 'changedocs'
		, 'creditor'

		, 'addr_form'
		, 'addr_input'
		, 'addr_textarea'
		, 'addr_test'
		], Array.prototype.slice.call(arguments,1));
	}
);