define([
	  'forms/base/controller'
	, 'forms/base/b_object'
	, 'forms/base/h_constraints'
],
function (BaseController, CreateBinding, h_constraints)
{
	return function (view_template, options)
	{
		var controller = BaseController();

		controller.GetViewTemplate = function () { return view_template; };

		if (options && options.constraints && !options.constraints_by_fields)
			options.constraints_by_fields = h_constraints.build_by_fields(options.constraints);

		controller.Render = function (form_div_selector)
		{
			var form_div = $(form_div_selector);

			var view_template = this.GetViewTemplate();

			this.binding = (!options || !options.CreateBinding) ? CreateBinding() : options.CreateBinding();
			this.binding.controller = this;
			this.binding.form_div_selector = form_div_selector;
			this.binding.model = controller.model;
			this.binding.options = options;
			if (options && options.foreign_data)
				this.binding.foreign_data = options.foreign_data;

			if (this.binding.PrepareBindingsForFields_BeforeApplyHtmlTemplate)
				this.binding.PrepareBindingsForFields_BeforeApplyHtmlTemplate();

			form_div.html(view_template(this.binding));

			if (this.binding.LoadFromModel_JsWidgets)
				this.binding.LoadFromModel_JsWidgets();

			if (this.binding.on_after_render)
				this.binding.on_after_render();
			if (this.binding.b_field)
			{
				for (var field_name in this.binding.b_field)
				{
					var b_field = this.binding.b_field[field_name];
					if (b_field.on_after_render)
						b_field.on_after_render();
				}
			}
		};

		controller.GetFormContent = function ()
		{
			var res = this.binding.model ? this.binding.model : 
				this.binding.CurrentCollectionIndex >= 0 ? [] : {};
			this.binding.SaveFieldsToModel(this.binding.form_div_selector, res);
			return res;
		}

		controller.SetFormContent = function (content)
		{
			this.model = ("string" != typeof content) ? content : JSON.parse(content);
		}

		controller.CreateNew = function (sel) { this.Render(sel); };
		controller.Edit = function (sel) { this.Render(sel); }

		controller.Validate = function ()
		{
			if (this.binding.CurrentCollectionIndex >= 0)
			{
				return null;
			}
			else
			{
				return this.binding.Validate(this.binding.form_div_selector);
			}
		}

		return controller;
	}
});
