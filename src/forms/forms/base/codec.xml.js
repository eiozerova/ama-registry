define([
	  'forms/base/codec'
	, 'forms/base/log'
],
function (BaseCodec, GetLogger)
{
	var log = GetLogger('codec.xml');
	return function ()
	{
		var res = BaseCodec();

		res.formatted = true;
		res.tabs = ['', '\t', '\t\t', '\t\t\t', '\t\t\t\t', '\t\t\t\t\t', '\t\t\t\t\t\t', '\t\t\t\t\t\t\t', '\t\t\t\t\t\t\t\t', '\t\t\t\t\t\t\t\t\t'];
		res.skipComments = true;
		res.FormatTabs = function (num)
		{
			if (!this.formatted)
			{
				return '';
			}
			else
			{
				return this.tabs[num];
			}
		};

		res.EncodeStringForXml = function (txt)
		{
			return txt.replace("&", "&amp;").replace("<", "&lt;").replace("<", "&gt;");
		};

		res.FormatEOL = function ()
		{
			if (!this.formatted)
			{
				return '';
			}
			else
			{
				return '\r\n';
			}
		};

		res.OpenTag = function (name)
		{
			return '<' + name + '>';
		};

		res.CloseTag = function (name)
		{
			return '</' + name + '>';
		};

		res.OpenTagLine = function (num_tabs, name)
		{
			return this.FormatTabs(num_tabs) + this.OpenTag(name) + this.FormatEOL();
		};

		res.CloseTagLine = function (num_tabs, name)
		{
			return this.FormatTabs(num_tabs) + this.CloseTag(name) + this.FormatEOL();
		};

		res.GetTagNameWithoutNamespace = function (node)
		{
			if (!node.tagName)
			{
				return null;
			}
			else
			{
				var parts = node.tagName.split(':');
				return 1 == parts.length ? parts[0] : parts[1];
			}
		};

		res.StringifyField = function (num_tabs, field_name, field_value, comment)
		{
			var res =
				this.FormatTabs(num_tabs) +
				this.OpenTag(field_name) +
				this.EncodeStringForXml(field_value + "") +
				this.CloseTag(field_name);
			if (comment && !this.skipComments)
				res += '<!-- ' + comment + '-->';
			res += this.FormatEOL();
			return res;
		};

		res.SafeStringifyField = function (num_tabs, field_name, field_value, comment)
		{
			return (!field_value) ? '' : this.StringifyField(num_tabs, field_name, field_value, comment);
		};

		res.DecodeChildNodes = function (node, on_child)
		{
			var childs = node.childNodes;
			var childs_len = childs.length;
			for (var i = 0; i < childs_len; i++)
			{
				var child = childs[i];
				on_child(child);
			}
		};

		res.TraceException = function (ex)
		{
			if ('object' != typeof ex)
			{
				log.Error('string exception:');
				log.Error(ex);
			}
			else
			{
				log.Error('object exception:');
				log.Error('ex.length=' + ex.length);
				for (var name in ex)
				{
					log.Error('.');
					log.Error(name + ':' + ex[name]);
				}
			}
		}

		res.Decode = function (xml_string)
		{
			var doc = null;
			try
			{
				if (window && window.DOMParser)
				{
					parser = new DOMParser();
					doc = parser.parseFromString(xml_string, "text/xml");
				}
				else // Internet Explorer
				{
					doc = new ActiveXObject('MSXML2.DOMDocument.4.0');
					doc.async = 'false';
					doc.loadXML(xml_string);
				}
			}
			catch (ex)
			{
				log.Error('exception on parse xml..');
				this.TraceException(ex);
				doc = ex;
			}
			if (doc && doc.documentElement && !doc.getElementsByTagName('parsererror').length)
			{
				return this.DecodeXmlDocument(doc);
			}
			else
			{
				log.Error('wrong xml:');
				if (!doc)
				{
					log.Error('!doc');
				}
				else
				{
					log.Error(xml_string);
					if (!doc.documentElement)
						log.Error('!doc.documentElement');
					log.Error(doc.getElementsByTagName('parsererror'));
					log.Error(doc.parsererror);
					if (!doc.documentElement)
						log.Error('!doc.documentElement');
				}
				return null;
			}
		};

		res.DecodeXmlElement = function (element, schema)
		{
			log.Debug('DecodeXmlElement {');
			var model = null;
			var childs = element.childNodes;
			var childs_len = childs.length;
			for (var i = 0; i < childs_len; i++)
			{
				var child = childs[i];
				if (child.tagName)
				{
					var tname = child.tagName;
					if (null == model)
					{
						if (schema && schema.type && 'array' == schema.type)
						{
							model = [this.DecodeXmlElement(child, schema.item)];
						}
						else
						{
							model = {};
							var field_schema = (!schema || !schema.fields) ? null : schema.fields[tname];
							var field_value = this.DecodeXmlElement(child, field_schema);
							model[tname] = field_value;
						}
					}
					else
					{
						if ('[object Array]' === Object.prototype.toString.call(model))
						{
							var item_schema = !schema ? null : schema.item;
							model.push(this.DecodeXmlElement(child, item_schema));
						}
						else
						{
							if (model[tname])
							{
								var item_schema = !schema ? null : schema.item;
								model = [model[tname], this.DecodeXmlElement(child, item_schema)];
							}
							else
							{
								var field_schema = (!schema || !schema.fields) ? null : schema.fields[tname];
								model[tname] = this.DecodeXmlElement(child, field_schema);
							}
						}
					}
				}
			}
			if (null == model)
				model = this.GetElementText(element); // ��� IE �������� ������ text..
			log.Debug('DecodeXmlElement }');
			return model;
		}

		res.GetElementText= function(element)
		{
			return element.text ? element.text : element.textContent;
		}

		res.DecodeXmlDocument = function (doc)
		{
			log.Debug('DecodeXmlDocument {');
			var root = doc.documentElement;
			var model = this.DecodeXmlElement(root, this.schema);
			log.Debug('DecodeXmlDocument }');
			return model;
		};

		res.EncodeDataItem = function (num_tabs, field_value, field_name, schema)
		{
			var xml_string = '';
			if ((schema && schema.type && 'array' == schema.type) ||
				'[object Array]' === Object.prototype.toString.call(field_value))
			{
				xml_string += this.OpenTagLine(num_tabs, field_name);
				var item_schema = (!schema || !schema.item) ? null : schema.item;
				xml_string += this.EncodeDataArray(num_tabs + 1, field_value, item_schema);
				xml_string += this.CloseTagLine(num_tabs, field_name);
			}
			else
			{
				var typeof_field_value = (typeof field_value);
				if ('object' == typeof_field_value)
				{
					xml_string += this.OpenTagLine(num_tabs, field_name);
					xml_string += this.EncodeDataObject(num_tabs + 1, field_value, schema);
					xml_string += this.CloseTagLine(num_tabs, field_name);
				}
				else if ('string' == typeof_field_value)
				{
					xml_string += this.StringifyField(num_tabs, field_name, field_value);
				}
			}
			return xml_string;
		};

		res.EncodeDataArray = function (num_tabs, data_array, schema)
		{
			var xml_string = '';
			for (var i = 0; i < data_array.length; i++)
			{
				var field_value = data_array[i];
				var item_name = this.getTagName(schema, 'item');
				xml_string += this.EncodeDataItem(num_tabs, field_value, item_name, schema);
			}
			return xml_string;
		};

		res.EncodeDataObject = function (num_tabs, data_object, schema)
		{
			var xml_string = '';
			for (var field_name in data_object)
			{
				var field_value = data_object[field_name];
				var field_schema = (!schema || !schema.fields) ? null : schema.fields[field_name];
				xml_string += this.EncodeDataItem(num_tabs, field_value, field_name, field_schema);
			}
			return xml_string;
		};

		res.getTagName = function (schema, default_name)
		{
			return (!schema || !schema.tagName) ? default_name : schema.tagName;
		}

		res.Encode = function (data)
		{
			log.Debug('Encode {');
			var xml_string = '<?xml version="1.0" encoding="utf-8"?>' + this.FormatEOL();
			xml_string += this.EncodeDataItem(0, data, this.getTagName(this.schema, 'root'), this.schema);
			log.Debug('Encode }');
			return xml_string;
		};

		return res;
	}
});
