define([
	  'forms/base/b_tools'
],
function (b_tools)
{
	return function ()
	{
		var binding =
		{
			get_Marker_class: function(p)
			{
				if (!p)
					p = ' ';
				return (!this.options || null == this.options || !this.options.Marker_class || null == this.options.Marker_class || '' == this.options.Marker_class)
					? '' : (p + this.options.Marker_class);
			},

			empty_attrs: function ()
			{
				var res = (!this.model || 0 == this.model.length) ? '' : ' style="display:none" ';
				res += 'class="empty-list"';
				return res;
			},

			not_empty_attrs: function ()
			{
				var res = (this.model && 0 != this.model.length) ? '' : ' style="display:none" ';
				res += 'class="not-empty-list' + this.get_Marker_class() + '"';
				return res;
			},

			iterate: function ()
			{
				if (!this.CurrentCollectionIndex && 0 != this.CurrentCollectionIndex)
				{
					this.CurrentCollectionIndex = -1;
					return true;
				}
				else
				{
					this.CurrentCollectionIndex++;
					var model_len = !this.model ? 0 : this.model.length;
					return this.CurrentCollectionIndex < model_len;
				}
			},

			is_seed: function ()
			{
				return -1 == this.CurrentCollectionIndex;
			},

			item_attrs: function ()
			{
				if (-1 == this.CurrentCollectionIndex)
				{
					return ' style="display:none" class="seed' + this.get_Marker_class() + '" ';
				}
				else
				{
					var classes = '';
					if (this.options.BuildClassesForItem)
						classes = this.options.BuildClassesForItem(this.model,this.CurrentCollectionIndex);
					return ' class="collection-item' + this.get_Marker_class() + ' ' + classes + '" collection-index="' + this.CurrentCollectionIndex + '"';
				}
			},

			text_func_attrs: function (prepare_text_by_item)
			{
				if (!this.collection_item_text_funcs)
					this.collection_item_text_funcs = [];
				this.collection_item_text_funcs.push(prepare_text_by_item);
				return ' class="collection-item-text-' + (this.collection_item_text_funcs.length - 1) + this.get_Marker_class() + '"';
			},

			text_field_attrs: function (item_field_name)
			{
				return this.text_func_attrs
				(
					function (item)
					{
						return item[item_field_name];
					}
				);
			},

			field: function (item_field_name)
			{
				return this.is_seed() ? '' : this.model[this.CurrentCollectionIndex][item_field_name];
			},

			get_model_item_field_value: function (item_field_name)
			{
				return b_tools().get_model_field_value(this.model[this.CurrentCollectionIndex], item_field_name);
			},

			txt: function (item_field_name)
			{
				var value = '';
				if (!this.is_seed())
					value = this.get_model_item_field_value(item_field_name);
				var fixed_value = (!value || null == value) ? '' : value.toString().replace(/\"/g, '&quot;');
				return fixed_value + '" model_field_name="' + item_field_name;
			},

			checkbox_attrs: function (item_field_name)
			{
				var res = ' model_field_name="' + item_field_name + '"';
				if (!this.is_seed())
				{
					var actual_value = this.get_model_item_field_value(item_field_name);
					res += !actual_value ? "" : " checked='checked'";
				}
				return res;
			},

			select_attrs: function (item_field_name)
			{
				this.CurrentFieldName = item_field_name;
				return ' model_field_name="' + item_field_name + '"';
			},

			option_attrs: function (option_value)
			{
				
				var res = " value='" + option_value + "'";
				if (!this.is_seed())
				{
					var item_field_name = this.CurrentFieldName;
					var selected_value = this.get_model_item_field_value(item_field_name);
					if (option_value == selected_value)
						res = "selected='selected'" + res;
				}
				return res;
			},

			text_field: function (item_field_name)
			{
				var res = '<span ' + this.text_field_attrs(item_field_name) + ' >';
				if (!this.is_seed())
					res += this.model[this.CurrentCollectionIndex][item_field_name];
				return res;
			},

			text_field_end: function ()
			{
				return '</span>';
			},

			item_position1: function()
			{
				var res = '<span ' + this.text_func_attrs(function(item,i_item){return i_item+1;}) + ' >';
				if (!this.is_seed())
					res += '' + (this.CurrentCollectionIndex + 1);
				return res;
			},

			item_position1_end: function ()
			{
				return '</span>';
			},

			item_control: function()
			{
				if (!this.options || !this.options.BuildControllerForItem)
				{
					return this.text_for_absent_field();
				}
				else
				{
					return '<div class="collection-item-control' + this.get_Marker_class() + '"></div>';
				}
			},

			LoadFromModel_JsWidgets: function()
			{
				$(this.form_div_selector + ' div.collection-item-control').each(function ()
				{
					var ditem = $(this);
					if (ditem.hasClass('empty'))
					{
						var i_item = parseInt(obj.find_binding_for_item($(ditem)).i_item);
						var item = this.model[i_item];

						ditem.removeClass('empty');
					}
				});
			},

			text_for_absent_field: function (field_name)
			{
				var res = 'Can not find control spec for item';
				if (!this.options)
					res += ' (!this.options)';
				else if (!this.options.BuildControllerForItem)
					res += ' (!this.options.BuildControllerForItem)';
				return res;
			},

			safe_prepare_dom_item: function (i_item)
			{
				var dom_item = $(this.form_div_selector + ' *' + this.get_Marker_class('.') + '[collection-index="' + i_item + '"]');
				if (dom_item.length)
				{
					return dom_item;
				}
				else
				{
					var seed = $(this.form_div_selector + ' ' + this.get_Marker_class('.') + '.not-empty-list ' + this.get_Marker_class('.') + '.seed');
					var clone = seed.clone();
					clone.attr('collection-index', i_item);
					clone.removeClass('seed');
					clone.addClass('collection-item');
					clone.insertAfter(0 == i_item ? seed : $(this.form_div_selector + ' *' + this.get_Marker_class('.') + '[collection-index="' + (i_item - 1) + '"]'));
					clone.show();
					return clone;
				}
			},

			find_binding_for_item: function(dom_item)
			{
				var i_item;
				do
				{
					dom_item = dom_item.parent();
					i_item = dom_item.attr('collection-index');
				}
				while (dom_item && (!i_item || null==i_item));
				var res = { dom_item: dom_item };
				if (i_item && null!=i_item)
					res.i_item = i_item;
				return res;
			},

			fill_dom_item: function (i_item, model_item, dom_item)
			{
				dom_item.removeClass('deprecated');
				if (this.collection_item_text_funcs)
				{
					for (var i = 0; i < this.collection_item_text_funcs.length; i++)
					{
						var prepare_text_by_item = this.collection_item_text_funcs[i];
						var txt = prepare_text_by_item(model_item, i_item);
						var id_dom_item = dom_item.attr('id');
						var sel = this.form_div_selector + ' *[collection-index="' + i_item + '"]' + ' ' + this.get_Marker_class('.') + '.collection-item-text-' + i;
						$(sel).text(txt);
					}
				}
				if (this.options.BuildClassesForItem)
				{
					var classes = this.options.BuildClassesForItem(this.model, i_item);
					dom_item.attr('class','collection-item ' + classes);
				}
			},

			update_dom: function ()
			{
				var selector_not_empty = this.form_div_selector + ' ' + this.get_Marker_class('.') + '.not-empty-list';
				var dom_empty = $(this.form_div_selector + ' ' + this.get_Marker_class('.') + '.empty-list');
				var dom_not_empty = $(selector_not_empty);
				$(this.form_div_selector + ' ' + this.get_Marker_class('.') + '.collection-item').addClass('deprecated');
				if (!this.model || 0 == this.model.length)
				{
					dom_empty.show();
					dom_not_empty.hide();
				}
				else
				{
					dom_empty.hide();
					dom_not_empty.show();
					var seed = $(selector_not_empty + ' ' + this.get_Marker_class('.') + '.seed');
					for (var i = 0; i < this.model.length; i++)
					{
						var dom_item = this.safe_prepare_dom_item(i);
						var model_item = this.model[i];
						this.fill_dom_item(i, model_item, dom_item);
					}
				}
				$(this.form_div_selector + ' ' + this.get_Marker_class('.') + '.deprecated').remove();
				if (this.on_after_update_dom)
					this.on_after_update_dom();
			},

			SaveFieldsToModel: function (sel, model)
			{
				for (var i= 0; i<model.length; i++)
					b_tools(this.form_div_selector).SaveFieldsToModel(sel + ' .collection-item[collection-index="' + i + '"]', model[i]);
			}
		};
		return binding;
	}
});
