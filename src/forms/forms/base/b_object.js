define([
	  'forms/base/log'
	, 'forms/base/b_collection'
	, 'forms/base/h_constraints'
	, 'forms/base/h_msgbox'
	, 'forms/base/h_times'
	, 'forms/base/b_tools'
],
function (GetLogger, CreateCollectionBinding, h_constraints, h_msgbox, h_times, b_tools)
{
	return function ()
	{
		var log = GetLogger('b_object');

		var binding =
		{
			txt: function (field_name)
			{
				var value = this.get_model_field_value(field_name);
				var fixed_value = (!value || null == value) ? '' : value.toString().replace(/\"/g, '&quot;');
				return fixed_value + '" model_field_name="' + field_name;
			},

			select2_attrs: function (field_name)
			{
				var select2_fields = !this.select2_fields ? {} : this.select2_fields;
				this.select2_fields = select2_fields;

				var select2_field = !select2_fields[field_name] ? {} : select2_fields[field_name];
				select2_fields[field_name] = select2_field;

				return ' model_field_name="' + field_name + '"';
			},

			PrepareBindingsForFields_BeforeApplyHtmlTemplate: function ()
			{
				if (this.options && this.options.field_spec)
				{
					for (var field_name in this.options.field_spec)
					{
						var field_spec = this.options.field_spec[field_name];
						if (field_spec.type && 'collection' == field_spec.type)
						{
							if (!this.b_field)
								this.b_field = {};
							var b_collection = CreateCollectionBinding();
							this.b_field[field_name] = b_collection;
							if (this.model && this.model[field_name])
								b_collection.model = this.model[field_name];
						}
					}
				}
			},

			text_for_absent_field: function (field_name)
			{
				var res = 'Can not find control spec for field "' + field_name + '"';
				if (!this.options)
					res += ' (!this.options)';
				else if (!this.options.field_spec)
					res += ' (!this.options.field_spec)';
				else if (!this.options.field_spec[field_name])
					res += ' (!this.options.field_spec[field_name])';
				return res;
			},

			control: function (field_name)
			{
				if (!this.options || !this.options.field_spec || !this.options.field_spec[field_name])
				{
					return this.text_for_absent_field(field_name);
				}
				else
				{
					var field_spec = this.options.field_spec[field_name];
					if (field_spec.controller)
					{
						var controls = !this.controls ? {} : this.controls;
						this.controls = controls;

						var ccontrol = !controls[field_name] ? {} : controls[field_name];
						controls[field_name] = ccontrol;

						ccontrol.controller = field_spec.controller();

						return '<div model_field_name="' + field_name + '"></div>';
					}
					if (this.b_field[field_name])
					{
						var b_field = this.b_field[field_name];
						b_field.form_div_selector = this.form_div_selector + ' div[model_field_name="' + field_name + '"]';
						b_field.options = field_spec.options;

						var controls = !this.controls ? {} : this.controls;
						this.controls = controls;

						var ccontrol = !controls[field_name] ? {} : controls[field_name];
						controls[field_name] = ccontrol;

						return '<div model_field_name="' + field_name + '">' + ((!field_spec.tpl) ? '' : field_spec.tpl(b_field)) + '</div>';
					}
				}
			},

			fields_control: function (fields_name)
			{
				if (!this.options || !this.options.field_spec || !this.options.field_spec[fields_name])
				{
					return 'Can not find control spec for fields "' + fields_name + '"';
				}
				else
				{
					var field_spec = this.options.field_spec[fields_name];

					var controls = !this.fields_controls ? {} : this.fields_controls;
					this.fields_controls = controls;

					var ccontrol = !controls[fields_name] ? {} : controls[fields_name];
					controls[fields_name] = ccontrol;

					return '<div model_fields_name="' + fields_name + '"></div>';
				}
			},

			a_modal_attrs: function (field_name)
			{
				var a_modal_fields = !this.a_modal_fields ? {} : this.a_modal_fields;
				this.a_modal_fields = a_modal_fields;

				var a_modal_field = !a_modal_fields[field_name] ? {} : a_modal_fields[field_name];
				a_modal_fields[field_name] = a_modal_field;

				return ' model_field_name="' + field_name + '"';
			},

			value: function ()
			{
				return this.get_model_field_value(this.CurrentFieldName);
			},

			textarea_attrs: function (field_name)
			{
				this.CurrentFieldName = field_name;
				return ' model_field_name="' + field_name + '" ';
			},

			select_attrs: function (field_name)
			{
				this.CurrentFieldName = field_name;
				return ' model_field_name="' + field_name + '"';
			},

			option_attrs: function (option_value)
			{
				var field_name = this.CurrentFieldName;
				var selected_value = this.get_model_field_value(field_name);
				return (option_value != selected_value ? "" : "selected='selected'")
					+ " value='" + option_value + "'";
			},

			tabs_attrs: function ()
			{
				return ' renderas="tabs" ';
			},

			date_attrs: function ()
			{
				return ' renderas="date" ';
			},

			numbers_options: function (ifrom, ito)
			{
				var res = "";
				for (var i = ifrom; i < ito; i++)
				{
					res += "<option " + this.option_attrs(i) + " >" + i + "</option>";
				}
				return res;
			},

			get_model_field_value: function (field_name)
			{
				return (!this.model) ? '' : b_tools().get_model_field_value(this.model, field_name);
			},

			GetControlForField: function (field_name)
			{
				return $(this.form_div_selector + ' *[model_field_name="' + field_name + '"]');
			},

			GetControlForFields: function (fields_name)
			{
				return $(this.form_div_selector + ' *[model_fields_name="' + fields_name + '"]');
			},

			radio_attrs: function (field_name, value_to_check)
			{
				var res = ' model_field_name="' + field_name + '"';
				res += ' name="' + field_name + '"';
				res += ' value="' + value_to_check + '"';
				var actual_value = this.get_model_field_value(field_name);
				res += actual_value != value_to_check ? "" : " checked='checked'";
				return res;
			},

			checkbox_attrs: function (field_name, default_value)
			{
				var res = ' model_field_name="' + field_name + '"';
				var actual_value = this.get_model_field_value(field_name);
				if (true === actual_value || 'true' === actual_value)
				{
					res += " checked='checked'";
				}
				else if (false === actual_value || 'false' === actual_value)
				{
				}
				else if (true == default_value)
				{
					res += " checked='checked'";
				}
				else
				{
				}
				return res;
			},

			LoadFromModel_JsWidget_a_modal_text: function (field_name, item, field_options)
			{
				var field_value = null;
				var text= field_options.text;
				if (this.model && this.model[field_name])
				{
					field_value = this.model[field_name];
					if (null != text)
						item.text(text(field_value));
				}
				if (!field_options.controller)
				{
					item.on({ click: function (e) { e.preventDefault(); alert("unspecified controller!"); } });
				}
				else
				{
					var self= this;
					var validate = function (model_field_item) {
						var model_field_name = model_field_item.attr('model_field_name');
						if (model_field_name) {
							var model_field_div = self.FindFieldDiv(model_field_item);
							if (model_field_div)
								model_field_div.addClass('had_focus');
							self.ValidateField(model_field_name, model_field_item);
						}
					};
					var controller = 
					(	function (field_options, field_value, text, item)
						{
							return field_options.controller(field_value, function ()
							{
								field_value = self.model[field_name];
								if (null != text)
									item.text(text(field_value));
								validate(item);
							});
						}
					)
					(field_options, field_value, text, item);
					item.on({ click: (function (c_arg) { return function (e) { c_arg.ShowModal(e); } })(controller) });
				}
			},

			LoadFromModel_JsWidgets_a_modal_text: function ()
			{
				if (this.a_modal_fields)
				{
					for (var field_name in this.a_modal_fields)
					{
						var item = this.GetControlForField(field_name);
						if (this.options && this.options.field_spec && this.options.field_spec[field_name])
						{
							var field_options = this.options.field_spec[field_name];
							if ('function' === typeof field_options)
								field_options = field_options(this.model);
							this.LoadFromModel_JsWidget_a_modal_text(field_name, item, field_options);
						}
					}
				}
			},

			LoadFromModel_JsWidgets_control: function ()
			{
				if (this.controls)
				{
					for (var field_name in this.controls)
					{
						var ccontrol = this.controls[field_name];
						var item = this.GetControlForField(field_name);
						if (this.options && this.options.field_spec && this.options.field_spec[field_name])
						{
							var field_options = this.options.field_spec[field_name];
							if (field_options.controller)
							{
								var controller = field_options.controller();
								ccontrol.controller = controller;
								var value = this.get_model_field_value(field_name);
								if (value)
									controller.SetFormContent(value);
								var selector = this.form_div_selector + ' div[model_field_name="' + field_name + '"]';
								controller.Edit(selector);
							}
						}
					}
				}
			},

			LoadFromModel_JsWidgets_fields_control: function ()
			{
				if (this.fields_controls)
				{
					for (var fields_name in this.fields_controls)
					{
						var ccontrol = this.fields_controls[fields_name];
						var item = this.GetControlForFields(fields_name);
						if (this.options && this.options.field_spec && this.options.field_spec[fields_name])
						{
							var fields_options = this.options.field_spec[fields_name];
							if (fields_options.controller)
							{
								var controller = fields_options.controller();
								ccontrol.controller = controller;
								if (this.model)
									controller.SetFormContent(this.model);
								var selector = this.form_div_selector + ' div[model_fields_name="' + fields_name + '"]';
								controller.Edit(selector);
							}
						}
					}
				}
			},

			LoadFromModel_JsWidgets: function ()
			{
				if (this.select2_fields)
				{
					for (var field_name in this.select2_fields)
					{
						var item = this.GetControlForField(field_name);
						if (!this.options || !this.options.field_spec || !this.options.field_spec[field_name])
						{
							item.text('skipped options for select2 field "' + field_name + '"!');
						}
						else
						{
							item.select2(this.options.field_spec[field_name](this.model, field_name));
							if (this.model && this.model[field_name])
								item.select2('data', this.model[field_name]);
						}
					}
				}
				this.LoadFromModel_JsWidgets_control();
				this.LoadFromModel_JsWidgets_fields_control();
				this.LoadFromModel_JsWidgets_a_modal_text();
				this.SafeRender_Validation();
				$(this.form_div_selector + ' [renderas="tabs"]').tabs();

				var datepicker = $(this.form_div_selector + ' [renderas="date"]');
				datepicker.datepicker(h_times.DatePickerOptions);
				datepicker.change(h_times.OnConvertToDateFormat);
				this.UpdateAllTabsValidationState();
			},

			SaveToModel_Link: function (item_with_value, model, field_name)
			{
				if (this.CheckFirstLevelFieldItem(item_with_value))
				{
					if (!this.a_modal_fields || !this.a_modal_fields[field_name])
					{
						model[field_name] = item_with_value.val();
					}
					else
					{
						var value = !this.model || !this.model[field_name] ? null : this.model[field_name];
						model[field_name] = value;
					}
				}
			},

			SaveToModel_TextArea: function (item_with_value, model, field_name)
			{
				if (this.CheckFirstLevelFieldItem(item_with_value))
					this.SaveFieldValue(model, field_name, item_with_value.text());
			},

			SaveFieldValue: function (model, field_name, value)
			{
				var name_parts = field_name.split('.');
				for (var i = 0; i < name_parts.length - 1; i++)
				{
					var name_part = name_parts[i];
					if (!model[name_part])
						model[name_part] = {};
					model = model[name_part];
				}
				model[name_parts[name_parts.length - 1]] = $.trim(value);
			},

			CheckFirstLevelFieldItem: function (item_with_value)
			{
				var root = $(this.form_div_selector);
				var count = 0;
				for (var parent = item_with_value.parent(); count < 1000 && parent && null != parent && !parent.is(root); parent = parent.parent())
				{
					count++;
					var model_field_name = parent.attr('model_field_name');
					if (typeof model_field_name !== typeof undefined && model_field_name !== false)
						return false;
				}
				return true;
			},

			SafeDoWithField: function (item_with_value, model, func_to_save)
			{
				var field_name = item_with_value.attr('model_field_name');
				if (field_name)
				{
					func_to_save.call(this, item_with_value, model, field_name);
				}
			},

			SaveFieldsToModel_select2_fields: function (form_div_selector, model)
			{
				if (this.select2_fields)
				{
					for (var select2_field_name in this.select2_fields)
					{
						try
						{
							var select2_field_sel = this.form_div_selector + ' [model_field_name="' + select2_field_name + '"]';
							var field_div = $(select2_field_sel);
							var field_div_id = field_div.attr('id');
							var select2_item = $('#s2id_' + field_div_id);
							model[select2_field_name] = select2_item.select2('data');
						}
						catch (ex)
						{
							log.Error('can not get select2 field ' + select2_field_name);
							throw ex;
						}
					}
				}
			},

			SaveFieldsToModel_controls: function (form_div_selector, model)
			{
				if (this.controls)
				{
					for (var control_field_name in this.controls)
					{
						var ccontrol = this.controls[control_field_name];
						if (ccontrol.controller)
							model[control_field_name] = ccontrol.controller.GetFormContent();
					}
				}
			},

			SaveFieldsToModel_fields_controls: function (form_div_selector, model)
			{
				if (this.fields_controls)
				{
					for (var control_fields_name in this.fields_controls)
					{
						var ccontrol = this.fields_controls[control_fields_name];
						var fields = ccontrol.controller.GetFormContent();
						for (var field_name in fields)
							model[field_name] = fields[field_name];
					}
				}
			},

			SaveFieldsToModel_func: function (model, SaveToModel)
			{
				var self = this;
				return function (index) { self.SafeDoWithField($(this), model, SaveToModel); }
			},

			SaveFieldsToModel: function (form_div_selector, model)
			{
				var self = this;
				b_tools(form_div_selector).SaveFieldsToModel(form_div_selector, model);
				$(form_div_selector + ' textarea').each(self.SaveFieldsToModel_func(model, self.SaveToModel_TextArea));
				$(form_div_selector + ' a').each(self.SaveFieldsToModel_func(model, self.SaveToModel_Link));
				this.SaveFieldsToModel_select2_fields(form_div_selector, model);
				this.SaveFieldsToModel_controls(form_div_selector, model);
				this.SaveFieldsToModel_fields_controls(form_div_selector, model);
			},

			MarkByValidationResults: function (check_constraints_result, constraints, result_fields_validation)
			{
				for (var i = 0; i < constraints.length; i++)
				{
					var constraint = constraints[i];
					if (check_constraints_result == constraint.result || true == constraint.result)
						this.MarkByValidationResult(check_constraints_result, constraint, result_fields_validation);
				}
			},

			MarkByValidationResult: function (check_constraints_result, constraint, result_fields_validation)
			{
				var markfields = !constraint.marks ? constraint.fields : constraint.marks;
				for (var j = 0; j < markfields.length; j++)
				{
					var model_field_name = markfields[j];
					var result = result_fields_validation[model_field_name];
					if (result_fields_validation[model_field_name] == check_constraints_result || result_fields_validation[model_field_name] == constraint.result)
					{
						var model_field_item = $(this.form_div_selector + ' *[model_field_name="' + model_field_name + '"]');
						if (model_field_item)
						{
							model_field_item.addClass('had_focus');
							var model_field_div = this.FindFieldDiv(model_field_item);
							if (model_field_div)
							{
								model_field_div.addClass('had_focus');
								this.MarkByResult(result, model_field_item, model_field_div);
							}
						}
					}
				}
			},

			MarkByResult: function (check_constraints_result, model_field_item, model_field_div)
			{
				switch (check_constraints_result)
				{
					case false:
						model_field_div.addClass('invalid');
						model_field_div.removeClass('recommended');
						break;
					case true:
						model_field_div.removeClass('invalid');
						model_field_div.removeClass('recommended');
						break;
					case 'recommended':
						model_field_div.removeClass('invalid');
						model_field_div.addClass('recommended');
						break;
				}
				this.UpdateTabsValidationState(model_field_item.parents('div.ui-tabs-panel'));
			},

			ValidateFieldModel: function (model_field_name, model_field_item, model, model_constraints_statuses)
			{
				if (this.options && this.options.constraints_by_fields && this.options.constraints_by_fields[model_field_name])
				{
					var fields_to_validate = {};
					fields_to_validate[model_field_name] = true;
					var result_fields_validation = {};

					var self = this;
					var validate_field = function (model_field_name, model_field_item, model, model_constraints_statuses, fields_to_validate)
					{
						var model_field_div = self.FindFieldDiv(model_field_item);
						var constraints = self.options.constraints_by_fields[model_field_name];
						var check_constraints_result = h_constraints.check_constraints(model, constraints, model_constraints_statuses, fields_to_validate, model_field_name)
						fields_to_validate[model_field_name] = 'validated';
						return check_constraints_result;
					}

					var model_field_div = this.FindFieldDiv(model_field_item);
					var constraints = this.options.constraints_by_fields[model_field_name];
					var check_constraints_result = h_constraints.check_constraints(model, constraints, model_constraints_statuses, fields_to_validate, model_field_name)
					fields_to_validate[model_field_name] = 'validated';
					result_fields_validation[model_field_name] = check_constraints_result;
					
					for (var model_field_name2 in fields_to_validate)
					{
						if (true == fields_to_validate[model_field_name2])
						{
							var model_field_item2 = $(this.form_div_selector + ' *[model_field_name="' + model_field_name2 + '"]');
							result_fields_validation[model_field_name2] = validate_field(model_field_name2, model_field_item2, model, model_constraints_statuses, fields_to_validate);
						}
					}
					this.MarkByValidationResults(check_constraints_result, constraints, result_fields_validation);
				}
			},

			ValidateField: function (model_field_name, model_field_item)
			{
				var model = {};
				this.SaveFieldsToModel(this.form_div_selector, model);
				var model_constraints_statuses = {};
				this.ValidateFieldModel(model_field_name, model_field_item, model, model_constraints_statuses);
			},

			UpdateTabValidationState: function (tab)
			{
				var tab_id = tab.attr('id');
				var a_hint = $(this.form_div_selector + ' a[href="#' + tab_id + '"] .tab-hint');
				var invalid = tab.find('div.field.invalid');
				if (0 == invalid.length)
				{
					a_hint.removeClass('invalid');
					a_hint.removeClass('had_focus');
					var recommended = tab.find('div.field.recommended');
					if (0==recommended.length)
					{
						a_hint.removeClass('recommended');
					}
					else
					{
						a_hint.addClass('recommended');
					}
				}
				else
				{
					a_hint.removeClass('recommended');
					a_hint.addClass('invalid');
					if (0 != invalid.find('.had_focus').length)
					{
						a_hint.addClass('had_focus');
					}
					else
					{
						a_hint.removeClass('had_focus');
					}
				}
				
			},

			UpdateTabsValidationState: function (tabs)
			{
				if (tabs && null!=tabs && 0!=tabs.length)
				{
					for (var i = 0; i < tabs.length; i++)
						this.UpdateTabValidationState($(tabs[i]));
				}
			},

			UpdateAllTabsValidationState: function()
			{
				this.UpdateTabsValidationState($(this.form_div_selector + ' div.ui-tabs-panel'));
			},

			Validate: function()
			{
				var model = {};
				var model_constraints_statuses = {};
				var labels_for_fields = {};
				this.SaveFieldsToModel(this.form_div_selector, model);
				var failed_constraint_descriptions = [];

				if (this.options && this.options.constraints)
				{
					var constraints= this.options.constraints;
					for (var i= 0; i<constraints.length; i++)
					{
						var constraint = constraints[i];
						var check_constraint_result = h_constraints.check_constraint(model, constraint, model_constraints_statuses);
						constraint.result = check_constraint_result;
						if (true!==check_constraint_result)
						{
							var description = this.PrepareConstraintDescription(null, constraint, labels_for_fields)
							failed_constraint_descriptions.push({ description: description, check_constraint_result: check_constraint_result });

							var markfields = !constraint.marks ? constraint.fields : constraint.marks;
							for (var j = 0; j < markfields.length; j++)
							{
								var model_field_name = markfields[j];
								var model_field_item = $(this.form_div_selector + ' *[model_field_name="' + model_field_name + '"]');
								if (model_field_item)
								{
									model_field_item.addClass('had_focus');
									var model_field_div = this.FindFieldDiv(model_field_item);
									if (model_field_div)
									{
										model_field_div.addClass(false==check_constraint_result ? 'invalid' : 'recommended');
										model_field_div.addClass('had_focus');
									}
								}
							}
						}
					}
				}

				this.UpdateAllTabsValidationState();

				return 0 == failed_constraint_descriptions.length ? null : failed_constraint_descriptions;
			},

			FindFieldDiv: function(model_field_item)
			{
				return model_field_item.parents('.field');
			},

			FindFieldHint: function(model_field_div)
			{
				return model_field_div.find('div.field-hint');
			},

			FindFieldLabel: function(model_field_div)
			{
				return model_field_div.find('label:first-child');
			},

			FindLabelForFieldName: function(model_field_name)
			{
				var model_field_item = $(this.form_div_selector + ' *[model_field_name="' +model_field_name + '"]');
				var model_field_div= this.FindFieldDiv(model_field_item);
				var model_field_label= this.FindFieldLabel(model_field_div);
				return model_field_label.text();
			},

			SafeRender_Validation_text_field: function (model_field_name, model_field_item)
			{
				var self = this;
				var validate_text_input = function (model_field_item)
				{
					var model_field_name = model_field_item.attr('model_field_name');
					self.ValidateField(model_field_name, model_field_item);
				}
				var on_event_validate_if_had_focus = function (e)
				{
					var model_field_item = $(e.target);
					if (model_field_item.hasClass('had_focus'))
						validate_text_input(model_field_item);
				}
				model_field_item.change   (on_event_validate_if_had_focus);
				model_field_item.keyup	(on_event_validate_if_had_focus);
			},

			SafeRender_Validation_focusout_text_field: function (model_field_name, model_field_item)
			{
				var self = this;
				var validate_text_input = function (model_field_item)
				{
					var model_field_name = model_field_item.attr('model_field_name');
					self.ValidateField(model_field_name, model_field_item);
				}
				var on_focusout_validate= function (e)
				{
					var model_field_item = $(e.target);
					var model_field_name = model_field_item.attr('model_field_name');
					model_field_item.addClass('had_focus');
					var model_field_div = self.FindFieldDiv(model_field_item);
					if (model_field_div)
						model_field_div.addClass('had_focus');
					validate_text_input(model_field_item);
				}
				model_field_item.focusout (on_focusout_validate);
			},

			SafeRender_Validation_radio_field: function (model_field_name, model_field_item)
			{
				var self = this;
				var on_event_validate = function (e)
				{
					var model_field_item = $(e.target);
					var model_field_name = model_field_item.attr('model_field_name');
					var model_field_div = self.FindFieldDiv(model_field_item);
					if (model_field_div)
						model_field_div.addClass('had_focus');
					self.ValidateField(model_field_name, model_field_item);
				}
				model_field_item.change(on_event_validate);
				model_field_item.focusout(on_event_validate);
			},

			SafeRender_Validation_select_field: function (model_field_name, model_field_item)
			{
				var self = this;
				var on_event_validate = function (e)
				{
					var model_field_item = $(e.target);
					var model_field_name = model_field_item.attr('model_field_name');
					var model_field_div = self.FindFieldDiv(model_field_item);
					if (model_field_div)
						model_field_div.addClass('had_focus');
					self.ValidateField(model_field_name, model_field_item);
				}
				model_field_item.change(on_event_validate);
			},

			SafeRender_Validation_div_field: function (model_field_name, model_field_item)
			{
				var self = this;
				var on_event_validate = function (e)
				{
					var model_field_item = $(e.target);
					var model_field_name = model_field_item.attr('model_field_name');
					if (model_field_name)
					{
						var model_field_div = self.FindFieldDiv(model_field_item);
						if (model_field_div)
							model_field_div.addClass('had_focus');
						self.ValidateField(model_field_name, model_field_item);
					}
				}
				model_field_item.on('change', on_event_validate);
			},

			SafeRender_Validation_a_field: function (model_field_name, model_field_item)
			{
				var self = this;
				var validate_value = function (model_field_item)
				{
					var model_field_name = model_field_item.attr('model_field_name');
					self.ValidateField(model_field_name, model_field_item);
				}
				var on_event_validate_if_had_focus = function (e)
				{
					var model_field_item = $(e.target);
					if (model_field_item.hasClass('had_focus'))
						validate_value(model_field_item);
				}
				var on_focusout_validate= function (e)
				{
					var model_field_item = $(e.target);
					var model_field_name = model_field_item.attr('model_field_name');
					model_field_item.addClass('had_focus');
					var model_field_div = self.FindFieldDiv(model_field_item);
					if (model_field_div)
						model_field_div.addClass('had_focus');
					validate_value(model_field_item);
				}
				model_field_item.focusout (on_focusout_validate);
				model_field_item.change   (on_event_validate_if_had_focus);
			},

			SafeFindLabelForFieldName: function(model_field_name,labels_for_fields)
			{
				if (!labels_for_fields[model_field_name])
					labels_for_fields[model_field_name]= this.FindLabelForFieldName(model_field_name);
				return labels_for_fields[model_field_name];
			},

			PrepareConstraintDescription: function (element, constraint, labels_for_fields)
			{
				var labels= [];
				labels.push(element);
				for (var i= 0; i<constraint.fields.length; i++)
				{
					var field_name = constraint.fields[i];
					labels.push(this.SafeFindLabelForFieldName(field_name, labels_for_fields));
				}
				return constraint.description.apply(null, labels);
			},

			ShowMessageForField: function(model_field_name)
			{
				if (this.options && this.options.constraints)
				{
					var descriptions= [];
					var constraints= this.options.constraints;
					var labels_for_fields= { };
					for (var i= 0; i<constraints.length; i++)
					{
						var constraint = constraints[i];
						for (var j= 0; j<constraint.fields.length; j++)
						{
							if (model_field_name==constraint.fields[j] && (false == constraint.result || 'recommended'==constraint.result))
							{
								descriptions.push(this.PrepareConstraintDescription(model_field_name, constraint, labels_for_fields));
								break;
							}
						}
					}
					if (descriptions && descriptions.length > 0)
					{
						h_msgbox.ShowModal({
							title: 'Пояснения для поля ввода ' + this.SafeFindLabelForFieldName(model_field_name, labels_for_fields)
							, html: '<ul><li>' + descriptions.join('</li><li>') + '</li></ul>'
							, width: 600
						});
					}
				}
			},

			SafeRender_Hint: function (model_field_name, model_field_item, model, model_constraints_statuses)
			{
				var self= this;
				var model_field_div = this.FindFieldDiv(model_field_item);

				var hint = this.FindFieldHint(model_field_div);
				hint.addClass('constraints');
				hint.click(function () { self.ShowMessageForField(model_field_name); });
			},

			SafeRender_Validation: function()
			{
				if (this.options && this.options.constraints_by_fields)
				{
					var self = this;
					var model = {};
					var model_constraints_statuses = {};
					var res = null;
					this.SaveFieldsToModel(this.form_div_selector, model);
					for (var model_field_name in this.options.constraints_by_fields)
					{
						var item = $(this.form_div_selector + ' *[model_field_name="' + model_field_name + '"]');
						if (null != item)
						{
							this.SafeRender_Hint(model_field_name, item, model, model_constraints_statuses);
							if (1 == item.length && 'text' == item.attr('type'))
							{
								this.SafeRender_Validation_focusout_text_field(model_field_name, item);
								if (!item.hasClass('event_focus'))
									this.SafeRender_Validation_text_field(model_field_name, item);
							}
							else if ('radio' == item.attr('type') || 'checkbox' == item.attr('type'))
							{
								this.SafeRender_Validation_radio_field(model_field_name, item);
							}
							else if (1 == item.length && 'SELECT' == item[0].tagName)
							{
								this.SafeRender_Validation_select_field(model_field_name, item);
							}
							else if (1 == item.length && 'DIV' == item[0].tagName)
							{
								this.SafeRender_Validation_div_field(model_field_name, item);
							}
							else if (1 == item.length && 'TEXTAREA' == item[0].tagName)
							{
								this.SafeRender_Validation_text_field(model_field_name, item);
								this.SafeRender_Validation_focusout_text_field(model_field_name, item);
							}
							else if (1 == item.length && 'A' == item[0].tagName)
							{
								this.SafeRender_Validation_a_field(model_field_name, item);
							}
						}
					}
				}
			}
		};
		return binding;
	}
});
