define([
	  'forms/base/codec.xml'
	, 'forms/base/log'
],
function (BaseCodec, GetLogger)
{
	return function ()
	{
		var log = GetLogger('codec.xsd.xml');
		var res = BaseCodec();

		res.LoadXsd = function (xsd_text)
		{
			var xsd_doc = new ActiveXObject('MSXML2.DOMDocument.6.0');
			var res = xsd_doc.loadXML(xsd_text);
			if (res)
			{
				return xsd_doc;
			}
			else
			{
				log.Error('loading of xsd scheme is failed!');
				return null;
			}
		}

		res.GetScheme = function ()
		{
			log.Debug('GetScheme() {');
			alert('unimplemented GetScheme');
			log.Debug('GetScheme() }');
		};

		res.GetRootNamespaceURI = function ()
		{
			log.Debug('GetScheme() {');
			alert('unimplemented GetRootNamespaceURI!');
			log.Debug('GetScheme() }');
			return 'unspecified namespaceURI';
		};

		res.LoadXmlDocument = function (xml_string)
		{
			try
			{
				doc = new ActiveXObject('MSXML2.DOMDocument.6.0');
				doc.async = 'false';
				if (this.GetScheme && null != this.GetScheme)
				{
					doc.validateOnParse = 'true';
					log.Debug('call this.GetScheme() {');
					var schemas = this.GetScheme();
					log.Debug('call this.GetScheme() }');
					doc.schemas = schemas;
				}
				doc.loadXML(xml_string);
			}
			catch (ex)
			{
				log.Debug('can not prepare xml reader with scheme:\r\n' + ex.description);
				throw ex;
			}
			return (!doc) ? null : doc;
		};

		res.CheckLoadedXmlDocument = function (doc)
		{
			if (!doc)
			{
				log.Debug('!doc');
			}
			else if (doc.parseError && 0 != doc.parseError.errorCode)
			{
				var exmsg = "\nReason: " + doc.parseError.reason +
				"\nLine: " + doc.parseError.line + "\n";
				var err = { parseException: true, readablemsg: exmsg };
				log.Debug('throw parsing error..');
				throw err;
			}
			else if (!doc.documentElement)
			{
				log.Debug('!doc.documentElement');
			}
			else if (doc.getElementsByTagName('parsererror').length)
			{
				log.Debug('doc.getElementsByTagName("parsererror").length');
				log.Debug(doc.getElementsByTagName('parsererror'));
			}
			else
			{
				log.Debug('check root namespace uri..');
				var namespaceURI = this.GetRootNamespaceURI();
				var namespaceURIEdit = !this.GetEditNamespaceURI ? '' : this.GetEditNamespaceURI();
				if (doc.documentElement.namespaceURI == namespaceURI || doc.documentElement.namespaceURI == namespaceURIEdit)
				{
					return true;
				}
				else
				{
					log.Debug('namespaceURI != doc.documentElement.namespaceURI');
					var err = { parseException: true, readablemsg: 'doc.documentElement.namespaceURI !=\r\n"' + namespaceURI + '"' };
					throw err;
				}
			}
			return false;
		};

		res.Decode = function (xml_string)
		{
			log.Debug('Decode() {');
			var doc = this.LoadXmlDocument(xml_string);
			if (!this.CheckLoadedXmlDocument(doc))
			{
				log.Debug('Decode() } return null');
				return null;
			}
			else
			{
				log.Debug('call DecodeXmlDocument() {');
				var res = this.DecodeXmlDocument(doc);
				log.Debug('call DecodeXmlDocument() }');
				log.Debug('Decode() }');
				return res;
			}
		};

		return res;
	}
});