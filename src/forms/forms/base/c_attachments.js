﻿define([
	  'forms/base/c_binded'
	, 'forms/base/log'
	, 'forms/base/h_names'
	, 'forms/fms/base//h_Select2'
	, 'forms/fms/base/h_DocumentType'
],
function (BaseController, GetLogger, helper_Names, helper_Select2, helper_DocumentType)
{
	return function (view_template, options)
	{
		var log = GetLogger('c_attachments');
		var controller = BaseController(view_template, options);

		controller.ShowAttachmentsPanel = false;

		controller.result_Data =
		{
			subject: '',
			body: null,
			attachments: [],
			note: ''
		}

		controller.UseCodec = function (codec)
		{
			log.Debug('UseCodec() {');

			var old_GetFormContent = this.GetFormContent;
			var old_SetFormContent = this.SetFormContent;

			this.GetFormContent = function ()
			{
				log.Debug('Wrapper around GetFormContent() {');
				var contentEncode = this.SignedContent ? this.SignedContent :
					codec.Encode(old_GetFormContent.call(controller));
				if (codec.GetScheme)
				{
					try
					{
						log.Debug("Wrapper around GetFormContent codec have GetScheme, let's check Decode.. ");
						log.Debug("Wrapper around GetFormContent call codec.Decode {");
						codec.Decode(contentEncode);
						log.Debug("Wrapper around GetFormContent call codec.Decode }");
					}
					catch (ex)
					{
						throw { parseException: true, readablemsg: "Не получается раскодировать закодированные данные..", innerException: ex };
					}
				}
				this.result_Data.subject = '';
				this.result_Data.body = contentEncode;
				this.result_Data.attachments = this.CollectAttachments();
				this.result_Data.note = $('.scan-note').val();
				log.Debug('Wrapper around GetFormContent() }');
				return this.result_Data;
			};

			this.SetFormContent = function (form_content)
			{
				log.Debug('Wrapper around SetFormContent {');
				var content = !form_content.body ? null : codec.Decode(form_content.body);
				var res = old_SetFormContent.call(controller, content);
				this.SetAttachments(form_content.attachments, form_content.note);
				log.Debug('Wrapper around SetFormContent }');
				return res;
			};

			var old_UsedCodecInfo = this.UsedCodecInfo;
			this.UsedCodecInfo =
			{
				Codec: codec,
				previous:
				{
					CodecInfo: old_UsedCodecInfo,
					GetFormContent: old_GetFormContent,
					SetFormContent: old_SetFormContent
				}
			};

			log.Debug('UseCodec() }');
		}

		controller.RenderSelect2 = function (select2_element, h_values)
		{
			select2_element.select2({
				placeholder: '',
				allowClear: true,
				query: function (query) {
					query.callback({ results: helper_Select2.FindForSelect(h_values.GetValues(), query.term) });
				}
			});
		}

		controller.RenderFormAttachments = function ()
		{
			if (this.ShowAttachmentsPanel)
			{
				$('.cpw-forms-attachments').append(GetScanTag());
				$('#add-scans').click(function (e) { OnAddScans(e); });
				LoadAttachments(this.attachments, this.note);
			}
		}

		var RenderSelectDocumentType = function(scanType)
		{
			var indexScanDocumentType = $('.scan_DocumentType').length;
			if (indexScanDocumentType > 0) {
				controller.RenderSelect2($('.scan_DocumentType').last(), helper_DocumentType);
				$('.scan_DocumentType').last().select2('data', scanType);
			}
		}

		var GetScanTag = function ()
		{
			return '\
				<div id="scan-list">\n\
					<div>\n\
						<a href="#null" id="add-scans" >Добавить скан документа..</a>\n\
					</div>\n\
					<div id="scans">\n\
					</div>\n\
					<div id="scans-notes"></div>\n\
					<div id="modal-dialog-scan">\n\
						<div id="modal-dialog-scan-content" class="image-content"></div>\n\
					</div>\n\
				</div>';
		}

		var LoadAttachments = function (attachments, note)
		{
			if (attachments)
			{
				for (var i = 0; i < attachments.length; i++)
				{
					var attachment = attachments[i];
					var htmlScans = PrepAttachmentHtml(attachment.Name, attachment.FileName, attachment.FileSize, attachment.Content);
					$('#scans').append(htmlScans);
					RenderSelectDocumentType(attachment.Description);
				}
				$('#scan-list .remove-doc').on('click', function (e) { OnDeleteAttachmentClick(e); });
				$('#scan-list .scan-doc').on('click', function (e) { OnShowScreenshotClick(e); });
				AddNoteFormScans(note);
			}
		}

		var OnAddScans = function (e)
		{
			e.preventDefault();
			var wax = CPW_Singleton();
			if (wax)
				var scans = wax.LoadScans('Файлы изображений|*.jpg;*.jpeg;*.png;*.bmp;*.tif;*.tiff', '{Formats: "jpeg", ResolutionDPI:{min:200,max:300}, colors: "Gray"}');
			if (scans)
			{
				for (var i = 0; i < scans.length; i++)
				{
					var scan = scans[i];
					var shortName = scan.path.split('\\');
					shortName = shortName[shortName.length - 1];
					var htmlScans = PrepAttachmentHtml(shortName, scan.path, scan.size, scan.content);
					$('#scans').append(htmlScans);
					RenderSelectDocumentType({ id: 103008, text: "Паспорт гражданина Российской Федерации" });
				}
				$('#scan-list .remove-doc').on('click', function (e) { OnDeleteAttachmentClick(e); });
				$('#scan-list .scan-doc').on('click', function (e) { OnShowScreenshotClick(e); });
				AddNoteFormScans('');
			}
		}

		var AddNoteFormScans = function(note)
		{
			note = !note ? '' : note;
			if ($('#scans-notes').html() == "")
			{
				$('#scans-notes').html('\
					<div>\n\
						<table>\n\
							<tr>\n\
								<td style="width:135px;"><span>Комментарий к документам:</span></td>\n\
								<td style="vertical-align: top;"><textarea class="scan-note w-full" maxlength="50000" rows="3">' + note + '</textarea></td>\n\
							</tr>\n\
						</table>\n\
					</div>');
			}
		}

		var PrepAttachmentHtml = function (name, filepath, size, content)
		{
			return '\
				<div class="doc-wrap w-full">\n\
					<div style="overflow:hidden;">\n\
						<span class="file" style="display: none;">' + filepath + '</span>\n\
						<span class="content" style="display: none;">' + content + '</span>\n\
						<a class="remove-doc right-edit" href="#" alt="удалить" title="удалить"></a>\n\
						<u class="file-name">' + name + '</u>\n\
						(<span class="size">' + size + '</span>&nbsp;<span class="numbering_bytes">' + helper_Names.GetNumberingBytes(size) + '</span>)\n\
						<table class="comment_holder">\n\
							<tr>\n\
								<td class="image" style="width:32px;">\n\
									<a class="scan-doc" href="#" style="padding-left:5px">\n\
										<img style="max-width:50px" src="images/scan.png" />\n\
									</a>\n\
								</td>\n\
								<td class="coment"><div class="inline_data_block">\n\
									<div class="column_edit scan_DocumentType" style="width:739px"></div>\n\
								</div></td>\n\
							</tr>\n\
						</table>\n\
					</div>\n\
				</div>';
		}

		var OnDeleteAttachmentClick = function (e)
		{
			e.preventDefault();
			$(e.target).closest('.doc-wrap').remove();
			if ($('#scans-notes').html() != "" && $('#scans .doc-wrap').length == 0)
			{
				$('#scans-notes').html('');
			}
		}

		var OnShowScreenshotClick = function (e)
		{
			e.preventDefault();
			var docWrap = $(e.target).closest('.doc-wrap');
			if (docWrap)
			{
				var content = docWrap.find('.content').html();
				$('#modal-dialog-scan').dialog({
					width: 600,
					height: 600,
					resizable: true,
					modal: true,
					title: docWrap.find('.file-name').html(),
					buttons: {
						'Закрыть': function () {
							$(this).dialog('close');
						}
					}
				});
				$('#modal-dialog-scan-content').html('<div class="attach-block"><img style="max-width:500px;" src="data:image/png;base64,' + content + '"/></div>');
			}
		}

		controller.CollectAttachments = function()
		{
			var attachments= [];
			$('.doc-wrap').each(function (counter)
			{
				if ($(this).data('virtual') !== 1)
				{
					var size= $(this).find('.size').html().toString();
					var filename = $(this).find('.file').html();
					var name= $(this).find('.file-name').html();
					var content= $(this).find('.content').html();
					var description = $(this).find('.scan_DocumentType').select2('data');
					attachments.push
					({
						Name: name,
						FileName: name,
						FileSize: size,
						Description: description,
						Format: 'text/binary',
						Content: content,
						Note: '',
						AttachmentType: 's'
					});
				}
			});
			return attachments;
		}

		controller.SetAttachments = function (attachments, note)
		{
			this.attachments = attachments;
			this.note = note;
			return null;
		}

		return controller;
	}
});