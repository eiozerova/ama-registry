﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace ama_win
{
    [ComVisible(true)]
    public partial class UCCreditor : UserControl
    {
        public string PathToIndex { get; set; }

        public UCCreditor()
        {
            InitializeComponent();
            webBrowser.ObjectForScripting = this;
        }

        public void LoadHtml()
        {
            webBrowser.Navigate(new Uri(PathToIndex, System.UriKind.Absolute));
        }

        string m_ContentToLoad = null;
        public string Content
        {
            get 
            {
                object res = webBrowser.Document.InvokeScript("global_external_AMA_GetCreditorToSave");
                return null==res ? null : res.ToString();
            }
            set
            {
                m_ContentToLoad = value;
            }
        }

        // метод, который вызывается из js при загрузке..
        public string LoadCreditor()
        {
            string res= m_ContentToLoad;
            m_ContentToLoad = null;
            return res;
        }
    }
}
