// данный файл содержит функции, предназначенные для взаимодействия ПАУ и раздела торгов

function safe_log(a) {
    try {
        if (console && console.log)
            console.log(a);
    } catch (e) {
    }
};

// получить информацию о кредиторе из ПАУ для загрузки в форму..
function global_external_AMA_LoadCreditor() {
    var res = { Наименование: "Коза патрикеевна" };
    res = window.external.LoadCreditor();
    if (!res || null == res) {
        res = {};
    } else {
        res = JSON.parse(res);
    }
    safe_log(res);
    return res;
};

// получить информацию из формы для передачи в ПАУ..
function global_external_AMA_GetCreditorToSave() {
    var res = form_spec.GetFormContent();
    res = JSON.stringify(res, null, '\t');
    safe_log('global_external_AMA_LoadCreditor {');
    safe_log(res);
    safe_log('global_external_AMA_LoadCreditor }');
    return res;
};

// перекрывает ajax транспорт..
$(function() {
    $.ajaxTransport
    (
        '+*',
        function(options, originalOptions, jqXHR) {
            var external_ajax_transport =
            {
                send: function(headers, completeCallback) {
                    window.ajax_callback = function(status, responceText) {
                        completeCallback(status, 200 === status ? 'success' : 'error', { text: responceText });
                    }
                    window.external.ajax(options);
                },
                abort: function() {
                }
            }
            return external_ajax_transport;
        }
    );
});

// получить список контрагентов из ПАУ
function global_external_AMA_GetContacts() {
    var res = [{ "stitle": "Иван", "inn": "231231231231", "ogrn": "66666666" }];
    res = window.external.LoadContacts();
    if (!res || null == res) {
        res = {};
    } else {
        res = JSON.parse(res);
    }
    safe_log(res);
    return res;
};

function global_external_AMA_SetContactId(id, fullName) {
    window.external.SetContactId(id, fullName);
}